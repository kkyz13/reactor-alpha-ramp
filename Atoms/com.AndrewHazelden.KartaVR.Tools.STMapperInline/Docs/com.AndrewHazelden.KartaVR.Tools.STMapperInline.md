# STMapperInline - v1.1 2023-05-19

Macro Wrapped by Andrew Hazelden <andrew@andrewhazelden.com>  
STMapper.fuse created by Jacob Danell <jacob@emberlight.se>  

## Overview

`STMapperInline` is an Effects Template for the Resolve Edit page. Now you can do ST Map/UV Pass warping live on the Resolve Edit page.

## Usage

**Step 1.** Install Reactor and add the STMapper fuse package to your Resolve based system. Install the "STMapperInline" macro as an Effects library item using the instructions in the "Installation" section below. Restart Resolve.

**Step 2.** Open a Resolve video editing timeline in the Edit page.

**Step 3.** Display the Effects Library tab, and switch to the "Toolbox > Effects > " section. Drag the "STMapperInline" entry onto a video clip in the timeline.

**Step 4.** Click on the video clip in the timeline and switch to the Inspector's Effects tab. Enter the filepath for an ST Map image in the Filename field. You may have to change the STmapperInline setting for "Crop" to "STMap Frame" if the default value of "Image Frame" doesn't give you the output you desire.

**Step 5.** The viewer window in the Edit page should update in real-time to show your live ST Map warped result.

**Step 6. Bonus:** If you are using the kvrReframe360Ultra fuse in the Fusion page, there is a 1-click "Output ST Map" checkbox that can quickly generate an ST Map template image which you can save to disk and recycle for later.

## Background Info

This macro works as a GroupOperator element that packages up an STMapper fuse, and an inline embedded Loader node into one object.

The Loader node exposes a Filename slot in the Inspector window you can use to select an ST Map image on disk.

## Installation

Templates for your Edit Page Effects Library are included:

	Templates/Edit/Effects/KartaVR/Warp/STMapperInline.setting
	Templates/Fusion/KartaVR/Warp/STMapperInline.setting

### Windows

On a Windows system, if Reactor is configured with the standard `Reactor:/` PathMap, you will find STMapperInline at the following installation location:

	C:\ProgramData\Blackmagic Design\DaVinci Resolve\Fusion\Reactor\Deploy\Templates\Edit\Effects\KartaVR\Warp\STMapperInline.setting

	C:\ProgramData\Blackmagic Design\DaVinci Resolve\Fusion\Reactor\Deploy\Templates\Fusion\KartaVR\Warp\STMapperInline.setting

### macOS

On a macOS system, if Reactor is configured with the standard `Reactor:/` PathMap, you will find STMapperInline at the following installation location:

	/Library/Application Support/Blackmagic Design/DaVinci Resolve/Fusion/Reactor/Deploy/Templates/Edit/Effects/KartaVR\Warp\STMapperInline.setting

	/Library/Application Support/Blackmagic Design/DaVinci Resolve/Fusion/Reactor/Deploy/Templates/Fusion/KartaVR/Warp/STMapperInline.setting


### Linux

On a Linux system, if Reactor is configured with the standard `Reactor:/` PathMap, you will find STMapperInline at the following installation location:

	/var/BlackmagicDesign/DaVinci Resolve/Fusion/Reactor/Deploy/Templates/Edit/Effects/KartaVR/Warp/STMapperInline.setting

	/var/BlackmagicDesign/DaVinci Resolve/Fusion/Reactor/Deploy/Templates/Fusion/KartaVR/Warp/STMapperInline.setting

