-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vGradientTimeStretch"
DATATYPE = "Gradient"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Gradient\\Temporal",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Time based operation on Gradient objects.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TimeStretcher",
})

function Create()
    -- [[ Creates the user interface. ]]
    InGradient = self:AddInput("Gradient", "Gradient", {
        LINKID_DataType = "Gradient",
        INP_SendRequest = false,
        INP_Required = false,
        LINK_Main = 1
    })

    InTime = self:AddInput("Time", "Time", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = -100.0,
        INP_MaxScale = 100.0,
        INP_Default = 0.0,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutGradient = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        -- InGradient:SetAttrs({LINK_Visible = visible})
        InTime:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local t = InTime:GetValue(req).Value
    local out = InGradient:GetSource(t, req:GetFlags())
    OutGradient:Set(req, out)
end
