# Folder Dialog.py 2022-06-16
# by Andrew Hazelden <andrew@andrewhazelden.com>

# Display a folder dialog using Python + UI Manager. This is an alternative to relying on a legacy AskUser dialog which only works in the Fusion page inside of Resolve.


ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

dlg = disp.AddWindow({'WindowTitle': 'Open Folder Dialog', 'ID': 'MyWin', 'Geometry': [100, 100, 500, 75],},[
	ui.VGroup({'Spacing': 0,},[
		# Add your GUI elements here:
		ui.HGroup({'Weight': 0.0,},[
			ui.Label({'ID': 'Label', 'Text': 'Foldername', 'Weight': 0.1}),
			ui.LineEdit({'ID': 'FolderLineTxt', 'Text': '', 'PlaceholderText': 'Please Enter a folder path', 'Weight': 0.9}),
			ui.Button({'ID': 'BrowseButton', 'Text': 'Browse', 'Geometry': [0, 0, 30, 50], 'Weight': 0.1}),
		]),
		ui.VGap(),
		ui.HGroup({'Weight': 0.1},[
			ui.Button({'ID': 'OpenButton', 'Text': 'Open Folder', 'Geometry': [0, 0, 30, 50], 'Weight': 0.1}),
		]),
	]),
])

itm = dlg.GetItems()

# The window was closed
def _func(ev):
	disp.ExitLoop()
dlg.On.MyWin.Close = _func

# Add your GUI element based event functions here:
def _func(ev):
	print('[Open Folder] Button Clicked')
	disp.ExitLoop()
dlg.On.OpenButton.Clicked = _func

def _func(ev):
	selectedPath = fu.RequestDir()
	if selectedPath:
		itm['FolderLineTxt'].Text = str(selectedPath)
dlg.On.BrowseButton.Clicked = _func

dlg.Show()
disp.RunLoop()
dlg.Hide()

# Expand relative filepaths using the Fusion based "MapPath" function:
folderpath = app.MapPath(itm['FolderLineTxt'].Text or '')
# Alternatively you could expand comp file specific PathMaps using:
# folderpath = comp.MapPath(itm['FolderLineTxt'].Text)

print('\n\n[Open Folder]', folderpath)
