-- Description: 
-- Create a Loader from selected Saver node. Works with multiple Savers.
-- Features:
-- If the Saver has image sequence with no number padding, scan existing files and suggest correct sequence numbering 
-- If the file is a container (mov, avi, mp4, mxf) — original name is used. 
-- Launch this scripts with Saver Manager UI tool (available in Reactor) 
-- connect the created Loader to Saver's downstream nodes 
-- convert regular saver to Saver Plus 
-- add versioning buttons to Saver Plus 
--
-- License: MIT
-- Author: Alexey Bogomolov
-- email: mail@abogomolov.com

comp = fu:GetCurrentComp()

selectedSavers = comp:GetToolList(true, 'Saver')

local platform = (FuPLATFORM_WINDOWS and 'Windows') or (FuPLATFORM_MAC and 'Mac') or (FuPLATFORM_LINUX and 'Linux')

local listDirCmd = "ls "
local suffixCmd = ""

if platform == "Windows" then
    listDirCmd = "dir "
    suffixCmd = " /b"
end

local VERBOSE = true
local scriptname = "LDfromSV"

-----------------------------------------------------------------------
-- Debug Logging
------------------------------------------------------------------------

function _log(mode, message)
	return string.format('[%s] - [%s]: %s', scriptname, mode, message)
end

function logError(message)
	print(_log('ERROR', message))
end

function logDebug(message, state)
	if state == 1 or state == true then
		-- Only print the logDebug output when the debugging 'state' is enabled
		print(_log('DEBUG', message))
	end
end

function logDump(variable, state)
	if state == 1 or state == true then
		-- Only print the logDump output when the debugging "state" is enabled
		dump(variable)
	end
end

function logWarning(message)
	print(_log('WARNING', message))
end

if comp:GetAttrs("COMPS_FileName") == "" then
    logWarning('Save the comp!', VERBOSE)
end

function isMovieFormat(extension)
	if extension ~= nil then
        if  ( extension == ".3gp" ) or
            ( extension == ".aac" ) or
            ( extension == ".aif" ) or
            ( extension == ".aiff" ) or
            ( extension == ".avi" ) or
            ( extension == ".dvs" ) or
            ( extension == ".fb" ) or
            ( extension == ".flv" ) or
            ( extension == ".m2ts" ) or
            ( extension == ".m4a" ) or
            ( extension == ".m4b" ) or
            ( extension == ".m4p" ) or
            ( extension == ".mkv" ) or
            ( extension == ".mov" ) or
            ( extension == ".mp3" ) or
            ( extension == ".mp4" ) or
            ( extension == ".mts" ) or
            ( extension == ".mxf" ) or
            ( extension == ".omf" ) or
            ( extension == ".omfi" ) or
            ( extension == ".qt" ) or
            ( extension == ".stm" ) or
            ( extension == ".tar" ) or
            ( extension == ".vdr" ) or
            ( extension == ".vpv" ) or
            ( extension == ".wav" ) or
            ( extension == ".webm" ) then
			return true
		end
	end
	return false
end

function findFiles(parsedPath)
    local path = parsedFile.Path
    path = '"' .. comp:MapPath(path) .. '"'
    files = io.popen(listDirCmd .. path .. suffixCmd) or nil
    if not files then
        return "0000"
    end
    for file in files:lines() do
        base, snum, ext = string.match(file,"^(.*[._-])(%d+)(%..+)$")
        if base == parsedPath.CleanName then
            return snum or "0000"
        end
    end
    files:close()
    return nil

end

function placeAndReconnect(name, tool)
    local flow = comp.CurrentFrame.FlowView
    x, y = flow:GetPos(tool)
    local loader = comp:AddTool("Loader")
    loader.Clip = name
    flow:SetPos(loader, x+1, y)
    inputs = tool.Output:GetConnectedInputs()
    for i, input in ipairs(inputs) do
        input:ConnectTo(loader.Output)
    end
    if not bmd.fileexists(comp:MapPath(name)) then
        print("file is not found ", name)
    end
end


------------------------------------------------------------------------------
-- ParseFilename() is from bmd.scriptlib
--
-- This is a great function for ripping a filepath into little bits
-- returns a table with the following
--
-- FullPath : The raw, original path sent to the function
-- Path : The path, without filename
-- FullName : The name of the clip w\ extension
-- Name : The name without extension
-- CleanName: The name of the clip, without extension or sequence
-- SNum : The original sequence string, or '' if no sequence
-- Number : The sequence as a numeric value, or nil if no sequence
-- Extension: The raw extension of the clip
-- Padding : Amount of padding in the sequence, or nil if no sequence
-- UNC : A true or false value indicating whether the path is a UNC path or not
------------------------------------------------------------------------------
-- Example: frameNumber = tonumber(ParseFilename(filename).Number)

function ParseFilename(filename)
	local seq = {}
	if filename == '' then
		logDebug('No filename specified!', VERBOSE)
		return seq
	end
	seq.FullPath = comp:MapPath(filename)
	string.gsub(seq.FullPath, '^(.+[/\\])(.+)', function(path, name) seq.Path = path seq.FullName = name end)
	string.gsub(seq.FullName, '^(.+)(%..+)$', function(name, ext) seq.Name = name seq.Extension = ext end)

	if not seq.Name then
		-- Note: This code running means there was no extension?
		seq.Name = seq.FullName
	end

	string.gsub(seq.Name, '^(.-)(%d+)$', function(name, SNum) seq.CleanName = name seq.SNum = SNum end)

	if seq.SNum then
		seq.Number = tonumber(seq.SNum)
		seq.Padding = string.len(seq.SNum)
	else
		seq.SNum = ''
		seq.CleanName = seq.Name
	end

	if seq.Extension == nil then seq.Extension = '' end
	seq.UNC = (string.sub(seq.Path, 1, 2) == [[\\]])

	return seq
end

function main()

    if (#selectedSavers) == 0 then
        logError('Select some savers')
        return
    end

    comp:Lock()
    comp:StartUndo('Loader from Saver')
    for _, tool in ipairs(selectedSavers) do
        selectedClipName = tool.Clip[1]
        if selectedClipName == "" then
            logWarning("Saver " .. tool.Name .. ": filename is empty!", VERBOSE)
        else
            selectedClipName = selectedClipName:gsub("%.%.(%w+)$", ".0000.%1")
            parsedFile = ParseFilename(selectedClipName)
            ext = parsedFile.Extension:lower()
            path = parsedFile.Path
            cleanName = parsedFile.CleanName
            sequenceNumber = parsedFile.SNum 

            -- if it is a sequence, find actual seq number
            if not isMovieFormat(ext) then
                if not sequenceNumber or not bmd.fileexists(comp:MapPath(selectedClipName)) then
                    suggestedSnum = findFiles(parsedFile)
                    if not suggestedSnum then
                        newClipName = selectedClipName
                    else
                        logWarning("Starting sequence number for the new Loader is set to: ".. suggestedSnum, VERBOSE)
                        newClipName = path .. cleanName .. suggestedSnum .. ext
                    end
                else
                    newClipName = path .. cleanName .. sequenceNumber .. ext
                end
            else
                newClipName = selectedClipName
            end

            placeAndReconnect(newClipName, tool)
        end
    end
    comp:EndUndo()
    comp:Unlock()
end

main()