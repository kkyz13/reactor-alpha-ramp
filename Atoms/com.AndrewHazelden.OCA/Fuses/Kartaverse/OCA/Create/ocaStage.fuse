-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "ocaStage"
DATATYPE = "ScriptVal"
MAX_INPUTS = 64

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "Number",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\OCA\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a new OCA datastream stage.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    -- Remove the default "Controls" page from the Fuse GUI
    self:RemoveControlPage("Controls")

    InWhich = self:AddInput("Which", "Which", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed = 1,
        INP_MaxAllowed = MAX_INPUTS,
        INP_MaxScale = 1,
        INP_Integer = true,
        IC_Steps = 1.0,
        IC_Visible = false
    })

    self:AddControlPage("Project", {CTID_DIB_ID  = "Icons.Tools.Tabs.Image"})
    InName = self:AddInput("Root Name", "Name", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1
    })
    InImageDimensionsNest = self:BeginControlNest("Image Dimensions", "ImageDimensionsNest", true, {})
        InWidth = self:AddInput("Width", "width", {
            LINKID_DataType = "Number",
            INPID_InputControl = "SliderControl",
            INP_MinScale = 1,
            INP_MaxScale = 8192,
            INP_Default = 1920,
            INP_Integer = true,
            LINK_Main = 1,
        })
        InHeight = self:AddInput("Height", "height", {
            LINKID_DataType = "Number",
            INPID_InputControl = "SliderControl",
            INP_MinScale = 1,
            INP_MaxScale = 8192,
            INP_Default = 1080,
            INP_Integer = true,
            LINK_Main = 2,
        })
        InShowSizeInput = self:AddInput("Show Size Inputs", "ShowSizeInput", {
            LINKID_DataType = "Number",
            INPID_InputControl = "CheckboxControl",
            INP_Integer = true,
            INP_Default = 0.0,
            INP_External = false,
            INP_DoNotifyChanged = true
        })
    self:EndControlNest()
    InTimeNest = self:BeginControlNest("Time", "TimeNest", true, {})
        InStartTime = self:AddInput("Start Time", "StartTime", {
            LINKID_DataType = "Number",
            INPID_InputControl = "SliderControl",
            INP_MinScale = 0,
            INP_MaxScale = 1000,
            INP_Default = 0,
            INP_Integer = true,
            LINK_Main = 3,
        })
        InEndTime = self:AddInput("End Time", "EndTime", {
            LINKID_DataType = "Number",
            INPID_InputControl = "SliderControl",
            INP_MinScale = 1,
            INP_MaxScale = 1000,
            INP_Default = 240,
            INP_Integer = true,
            LINK_Main = 4,
        })
        InFrameRate = self:AddInput("Frame Rate", "FrameRate", {
            LINKID_DataType = "Number",
            INPID_InputControl = "SliderControl",
            INP_MinScale = 1.0,
            INP_MaxScale = 240.0,
            INP_Default = 24.0,
            INP_Integer = false,
            LINK_Main = 5,
        })
        InShowTimeInput = self:AddInput("Show Time Inputs", "ShowTimeInput", {
            LINKID_DataType = "Number",
            INPID_InputControl = "CheckboxControl",
            INP_Integer = true,
            INP_Default = 0.0,
            INP_External = false,
            INP_DoNotifyChanged = true
        })
    self:EndControlNest()
    InLayer1 = self:AddInput("Layer 1", "layer1", {
        LINKID_DataType = "ScriptVal",
        INPID_InputControl = "ImageControl",
        INP_Required = false,
        LINK_Main = 10
    })
    self:AddControlPage("Color", {CTID_DIB_ID  = "Icons.Tools.Tabs.Color"})
    InR = self:AddInput("Red", "Red", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ColorControl",
        LINKS_Name = "Background",
        INP_MinScale = 0.0,
        INP_MaxScale = 1.0,
        INP_Default  = 0.0,
        IC_ControlGroup = 1,
        IC_ControlID = 0,
        LINK_Main = 6,
    })
    InG = self:AddInput("Green", "Green", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ColorControl",
        INP_MinScale = 0.0,
        INP_MaxScale = 1.0,
        INP_Default  = 0.0,
        IC_ControlGroup = 1,
        IC_ControlID = 1,
        LINK_Main = 7,
    })
    InB = self:AddInput("Blue", "Blue", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ColorControl",
        INP_MinScale = 0.0,
        INP_MaxScale = 1.0,
        INP_Default  = 0.0,
        IC_ControlGroup = 1,
        IC_ControlID = 2,
        LINK_Main = 8,
    })
    InA = self:AddInput("Alpha", "Alpha", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ColorControl",
        INP_MinScale = 0.0,
        INP_MaxScale = 1.0,
        INP_Default  = 0.0,
        IC_ControlGroup = 1,
        IC_ControlID = 3,
        LINK_Main = 9,
    })
    InShowColorInput = self:AddInput("Show Color Inputs", "ShowColorInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    OutScriptVal = self:AddOutput("Output", "Output", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function OnAddToFlow()
    --[[ Callback triggered when adding the Fuse to the flow view. ]]
    -- find highest existing input
    local highest_input = 1

    for i = 1, MAX_INPUTS do
        local input = self:FindInput("layer" .. tostring(i))

        if input == nil then
            break
        end

        highest_input = i
    end

    -- add inputs
    -- NOTE: start at 2, input 1 always exists
    for i = 2, highest_input do
        self:AddInput("Layer " .. i, "layer" .. i, {
            LINKID_DataType = "ScriptVal",
            INPID_InputControl = "ImageControl",
            LINK_Main = i + 9,
            INP_Required = false,
            INP_DoNotifyChanged = true,
            ICS_ControlPage = "Project"
        })
    end

    -- set slider maximum
    InWhich:SetAttrs({INP_MaxScale = highest_input, INP_MaxAllowed = highest_input})
end

function OnConnected(inp, old, new)
    --[[ Callback triggered when connecting a Fuse to the input of this Fuse. ]]
    local inp_nr = tonumber(string.match(inp:GetAttr("LINKS_Name"), "Layer (%d+)"))
    local max_nr = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    if inp_nr then
        -- add input if maximum inputs is not exceeded and connection is not empty
        if inp_nr >= max_nr and max_nr < MAX_INPUTS and new ~= nil then
            -- set slider maximum
            InWhich:SetAttrs({INP_MaxScale = inp_nr, INP_MaxAllowed = inp_nr})

            -- add extra input
            self:AddInput("Layer " .. (inp_nr + 1), "layer" .. (inp_nr + 1), {
                LINKID_DataType = "ScriptVal",
                INPID_InputControl = "ImageControl",
                LINK_Main = (inp_nr + 10),
                INP_Required = false,
                INP_DoNotifyChanged = true,
                ICS_ControlPage = "Project"
            })
        end
    end
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowColorInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InR:SetAttrs({LINK_Visible = visible})
        InG:SetAttrs({LINK_Visible = visible})
        InB:SetAttrs({LINK_Visible = visible})
        InA:SetAttrs({LINK_Visible = visible})
    elseif inp == InShowSizeInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InWidth:SetAttrs({LINK_Visible = visible})
        InHeight:SetAttrs({LINK_Visible = visible})
    elseif inp == InShowTimeInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InStartTime:SetAttrs({LINK_Visible = visible})
        InEndTime:SetAttrs({LINK_Visible = visible})
        InFrameRate:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local name = InName:GetValue(req).Value

    -- Image Size
    local w = math.floor(InWidth:GetValue(req).Value)
    local h = math.floor(InHeight:GetValue(req).Value)
    local startTime = math.floor(InStartTime:GetValue(req).Value)
    local endTime = math.floor(InEndTime:GetValue(req).Value)
    local frameRate = InFrameRate:GetValue(req).Value

    -- Color
    local r = InR:GetValue(req).Value
    local g = InG:GetValue(req).Value
    local b = InB:GetValue(req).Value
    local a = InA:GetValue(req).Value

    local tbl = {
        backgroundColor = {
            r,
            g,
            b,
            a
        },
        height = h,
        width = w,
        colorDepth = "U8",
        layers = {},
        name = name,
        ocaVersion = "1.2.0",
        originApp = "OCA Data Nodes",
        originAppVersion= "1.0",
        startTime = startTime,
        endTime = endTime,
        frameRate = frameRate
    }

    -- Scan the "layer#" inputs
    local input_count = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    for i = 1, input_count do
        -- get input from index
        local input = self:FindInput("layer" .. tostring(i))

        -- get table from input
        if input ~= nil then
            if input:GetSource(req.Time, req:GetFlags()) ~= nil and input:GetSource(req.Time, req:GetFlags()):GetValue() ~= nil then
                local inp_tbl = input:GetSource(req.Time, req:GetFlags()):GetValue() or {}
                if inp_tbl ~= nil then
                    table.insert(tbl.layers, inp_tbl)
                end
            end
        end
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
