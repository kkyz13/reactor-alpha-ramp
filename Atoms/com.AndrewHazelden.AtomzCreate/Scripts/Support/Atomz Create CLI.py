"""
AtomzCreate - 1.2 2023-10-16
By Andrew Hazelden <andrew@andrewhazelden.com

Convert a GitLab Reactor repository folder into individually zipped atomz packaged archives. The zipped packages are easier to use and manage with a NAS-based offline Reactor Package installation approach.

Script Usage:
1. Clone the either the Reactor or Vonk Ultra repo using the terminal:

mkdir -p $HOME/Documents/Git/
cd $HOME/Documents/Git/

git clone https://gitlab.com/WeSuckLess/Reactor.git
git clone https://gitlab.com/AndrewHazelden/Vonk

2. Run the Python script:

python3 AtomzCreate.py "/Users/vfx/Documents/Git/Reactor"
python3 AtomzCreate.py "/Users/vfx/Documents/Git/Vonk"

3. Move the zipped atom packages onto a shared NAS volume for use in an airgapped network based studio environment.


Example Terminal Output:
[Atomz Create]
[Atomz][Source Folder] /Users/vfx/Documents/Git/Reactor/Atoms
[Atomz][Dest Folder] /Users/vfx/Documents/Git/Reactor/Atomz
/Users/vfx/Documents/Git/Reactor/Atomz/com.AlbertoGZ.ColorLabels.zip (00 Minutes 01 Seconds)
/Users/vfx/Documents/Git/Reactor/Atomz/com.AlbertoGZ.ReloadLoaders.zip (00 Minutes 01 Seconds)
/Users/vfx/Documents/Git/Reactor/Atomz/com.AlexBogomolov.AttributeSpreadsheet.zip (00 Minutes 01 Seconds)
/Users/vfx/Documents/Git/Reactor/Atomz/com.AlexBogomolov.Bookmarker.zip (00 Minutes 01 Seconds)
/Users/vfx/Documents/Git/Reactor/Atomz/com.AlexBogomolov.FrameRanger.zip (00 Minutes 01 Seconds)
...


"""

import os, shutil, datetime, math, argparse

parser = argparse.ArgumentParser(
    description="""Convert a GitLab Reactor repository folder into individually zipped atomz packages archives."""
)
parser.add_argument("path", help = "The path to your GitLab Reactor repository folder.")

args = parser.parse_args()
repoPath = args.path

def AtomzCreate(repo):
	atomsPath = os.path.join(repo, "Atoms")
	zipPath = os.path.join(repo, "Atomz")
	
	print("[Atomz Create]")
	print("[Atomz][Source Folder]", atomsPath)
	print("[Atomz][Dest Folder]", zipPath)

	# Create the output folder
	if not os.path.exists(zipPath):
		try:
			print("\t[Atomz][Make Directory]", zipPath)
			os.makedirs(zipPath)
		except OSError as error:
			print("\t[Atomz][Make Directory Error]", zipPath)

	# Generate the atom package list
	for dirName in sorted(os.listdir(atomsPath)):
		# Ignore hidden files starting with a periopd
		if not dirName.startswith("."):
			startTimer = datetime.datetime.now()

			# Build the filename
			zipBasename = os.path.join(zipPath, dirName)
			fmt = "zip"
			srcPath = os.path.join(atomsPath, dirName)

			# Zip the sub-folder
			result = shutil.make_archive(zipBasename, fmt, root_dir = srcPath, base_dir = ".")

			endTimer = datetime.datetime.now()
			elapsedTime = (endTimer - startTimer).total_seconds()
			mins, secs = divmod(elapsedTime, 60)
			timeFormatted = "(" + str(math.ceil(mins)).zfill(2) + " Minutes " + str(math.ceil(secs)).zfill(2) + " Seconds)"
			print(result, timeFormatted)

if __name__ == "__main__":
	AtomzCreate(repoPath)
	print("[Done]")
