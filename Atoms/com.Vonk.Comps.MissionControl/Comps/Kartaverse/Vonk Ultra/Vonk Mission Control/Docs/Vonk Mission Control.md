# Vonk Mission Control

2022-05-28 11.56 PM

Demo Created by:  
Andrew Hazelden andrew@andrewhazelden.com  

## Overview

The "Mission Control" Vonk data node example simulates a small propellor-powered aircraft's cockpit dashboard by rendering a mix of digital and analog style gauges. The logic for the controls is wired together using Vonk Number and Text data types.

Phase Two of this project will connect a pre-recorded NMEA ASCII format GPS datastream text file as the external driver of the airplane gauges via NMEA GPGGA and GPRMC strings.

## Screenshot

![Mission Control](Mission_Control_in_Fusion.png)

## Notes

The `Vonk Mission Control Resolve v001.comp` file was modified for easier use with Resolve/Resolve Studio. 

This file has all of the Loader node filepaths changed over to use "Reactor:/" based relative PathMaps, in-place of the normal Fusion Studio centric "Comp:/" PathMap usage.

