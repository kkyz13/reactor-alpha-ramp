-- based upon Cedric's "Create All Nodes" Lua Script:
-- https://gist.github.com/cedricduriau/125cd3b84ab72cc1afc85ebfe943193c#file-fusion_createallnodes-lua

reg_map = fusion:GetRegList()  -- dict[int, Registry]

-- Total Nodes
node_count = 0
for _i, reg in ipairs(reg_map) do
    if reg.ID:match("^Fuse.pt")  then
        node_count = node_count + 1
    end
end
print("[PT] [Create All Data Nodes] ", node_count)

-- Add the nodes
node_count = 0
for _i, reg in ipairs(reg_map) do
    if reg.ID:match("^Fuse.pt")  then
				node_count = node_count + 1
        print("[" .. node_count .. "] ", reg.ID)
        comp:AddTool(reg.ID)
    end
end
