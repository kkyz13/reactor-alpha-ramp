-- Lenses can be looked up by the project.globallenses. or imagegroup.#.globallens parameter

-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "ptFocalLength"
DATATYPE = "Number"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "ScriptVal",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\PT\\Number",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Get the focal length from a PTGui PTS ScriptVal object.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.LightTrim",
})

function Create()
    -- [[ Creates the user interface. ]]
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })

    InIndex = self:AddInput("Index", "Index", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        -- INPID_InputControl = "SliderControl",
        INP_Default = 1,
        INP_Integer = true,
        INP_MinScale = 1,
        INP_MaxScale = 360,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MaxAllowed = 1000000,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutNumber = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then
            InIndex:SetAttrs({LINK_Visible = true})
        else
            InIndex:SetAttrs({LINK_Visible = false})
        end
    end
end

function get(t, key)
    --[[
        Returns the value of a key in a table.

        :param t: Table to get key value for.
        :type t: table

        :param key: Key to get value of.
        :type key: string

        :rtype: ?
    ]]
    local value = nil
    local found = false

    for k, v in pairs(t) do
        if k == key then
            value = v
            found = true
            break
        end
    end

    if not found then
        error(string.format("no key '%s' found in ScriptVal table", key))
    end

    return value
end

function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()
    local key = InIndex:GetValue(req).Value
    local num = 0

    -- Extract: project.globallenses
    if type(tbl) == "table" and tbl.pts and tbl.pts.project and tbl.pts.project.globallenses and type(tbl.pts.project.globallenses) == "table" then
         -- Clamp the index at the maximum range
        max_keys = tonumber(table.getn(tbl.pts.project.globallenses))
        if key > max_keys then
            key = max_keys
        end

        -- Extract the number element
        groupTbl = get(tbl.pts.project.globallenses, key)
        -- dump(groupTbl)

        -- Extract the image filename
        if type(groupTbl) == "table" and groupTbl.lens and groupTbl.lens.params and type(groupTbl.lens.params) == "table" then
            -- dump(groupTbl.lens.params)
            value = groupTbl.lens.params.focallength
            num = value
        end
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutNumber:Set(req, Number(num))
end
