local P, S, C, R, Ct, Cg, Cc = lpeg.P, lpeg.S, lpeg.C, lpeg.R, lpeg.Ct, lpeg.Cg, lpeg.Cc

local ws       = S ('\r\n\f\t ')^1
local alpha    = R ( "az", "AZ")
local digit    = R ( "09" )
local number   = (S ('+-')^-1 * digit^1 * (P "." * digit^0)^-1) / tonumber
local fuid     = (alpha + P "." + P "_" ) * (alpha + digit + P "." + P "_")^0
local rest     = (1-(ws * P "&"))^1

local prefix   = P "/for"
local scope    = Cg (P "all" + P "visible" + P "selected", "scope")
local tooltype = Cg (Ct(C(fuid) * (P "," * C(fuid))^0), "tooltype")
local color    = Ct(Cg(number, "R") * P"," * Cg(number, "G") * P"," * Cg(number, "B"))
local clrcolor = (P"clear" * Cc(false)) + color

function parseopts(cmds, str)
	local command = P(-1)

	for cmd,v in pairs(cmds) do
		command = command + (Cg(P(cmd), "command") * v[2])
	end

	command = Ct(command)
	local where = P"where" * ws * Cg((1 - command)^1, "where")

	local parser = Ct(prefix * ws * scope * ws * (command + (where * command) + (tooltype * ws * command)) * (ws * P "&" * ws * command) ^ 0 * ws^-1 * P(-1))

	return parser:match(str)
end

local commands =
{
	get =
	{
		"\tget <input> ([at <time>])",
		ws * Cg(fuid, "input") * (((ws * P"at" * ws * Cg(number, "time"))^-1)),
		function(tool, args)
			local inp = tool[args.input]

			if inp then
				local time = args.time or comp.CurrentTime
				local cur = inp[time]
				local cur_str
				if type(cur) == "string" then
					cur_str = "\"" .. tostring(cur) .. "\""
				elseif type(cur) == "table" then
					cur_str = tostring(bmd.writestring(cur))
				else
					cur_str = tostring(cur)
				end

				-- print(tostring(tool.Name) ..  "." .. tostring(args.input) .. " = " .. tostring(cur_str))
				print("[Tool] " .. tostring(tool.Name) ..  "\t[Input] " .. tostring(args.input) ..  "\t[Value] " .. tostring(cur_str))
			end
		end
	},
	getattrs =
	{
		"\tgetattrs <attribute>",
		ws * Cg(fuid, "attribute"),
		function(tool, args)
			local attrs = tool:GetAttrs()[args.attribute]
			if attrs ~= nil then
				local attrs_str
				if type(attrs) == "string" then
					attrs_str = "\"" .. tostring(attrs) .. "\""
				elseif type(attrs) == "table" then
					attrs_str = tostring(bmd.writestring(attrs))
				else
					attrs_str = tostring(attrs)
				end

				-- print(tostring(tool.Name) ..  ":GetAttrs()." .. tostring(args.attribute) .. " = " .. tostring(attrs_str))
				print("[Tool] " .. tostring(tool.Name) ..  "\t[Attr] " .. tostring(args.attribute) ..  "\t[Value] " .. tostring(attrs_str))
			end
		end
	},
	set =
	{
		"\tset <input> ([at <time>] to <value>|expression <exp>)",
		ws * Cg(fuid, "input") * (((ws * P"at" * ws * Cg(number, "time"))^-1 * ws * P"to" * ws * Cg(rest, "value")) + (ws * P"expression" * ws * Cg(rest, "expression"))),
		function(tool, args)
			local inp = tool[args.input]

			if inp then
				if args.expression then
					inp:SetExpression(args.expression)
				else
					local time = args.time or comp.CurrentTime
					local func,err = loadstring("return " .. args.value)

					if func then
						local cur = inp[time]
						setfenv(func, setmetatable({ value = cur, current = cur, time = time, tool = tool, input = inp }, { __index = _G }))
						inp[time] = func()
					else
						error(err)
					end
				end
			end
		end
	},
	setname =
	{
		"\tsetname (to <value>)",
		ws * P"to" * ws * Cg(rest, "value"),
		function(tool, args)
			if args.value then
				local command = (tostring(tool.Name) .. ":SetAttrs({TOOLS_Name = " .. tostring(args.value) .. " , TOOLB_NameSet = true})")
				-- print("[Tool Rename Command] " .. command)
				print("[Tool Rename] " .. tostring(tool.Name) .. " to " .. tostring(args.value))
				comp:Execute(command)
			end
		end
	},
	setclip =
	{
		"\tsetclip (to <value>)",
		ws * P"to" * ws * Cg(rest, "value"),
		function(tool, args)
			if args.value then
				local command = [=[
-- Disable the file browser dialog
local AutoClipBrowse = app:GetPrefs('Global.UserInterface.AutoClipBrowse')
app:SetPrefs('Global.UserInterface.AutoClipBrowse', false)

]=] ..(tostring(tool.Name) .. ".Clip[fu.TIME_UNDEFINED] = " .. tostring(args.value)) .. [=[
-- Re-enable the file browser dialog
app:SetPrefs('Global.UserInterface.AutoClipBrowse', AutoClipBrowse)
]=]
				-- print("[Loader Clip Command] " .. command)
				print("[Loader] " .. tostring(tool.Name) .. " [Clip] " .. tostring(args.value))
				comp:Execute(command)
			end
		end
	},
	setattrs =
	{
		"\tsetattrs <attribute> (to <value>)",
		ws * Cg(fuid, "attribute") * (ws * P"to" * ws * Cg(rest, "value")),
		function(tool, args)
			if args.attribute then
				local command = (tostring(tool.Name) .. ":SetAttrs({" .. tostring(args.attribute) .. " = " .. tostring(args.value) .. "})")
				-- print("[setattrs] [Command]  " .. command)
				-- print("[Tool] " .. tostring(tool.Name) .. " [Attr] " .. tostring(args.attribute) ..  "\t[Value] ", args.value)
				comp:Execute(command)
			end
		end
	},
	animate =
	{
		"\tanimate <input> [(with <modifier>|remove)] [force]",
		ws * Cg(fuid, "input") * ((ws * P"with" * ws * Cg(fuid, "modifier")) + (ws * Cg(P"remove", "remove")))^-1 * (ws * Cg(P"force", "force"))^-1,
		function(tool, args)
			local inp = tool[args.input]
			if inp then
				if args.remove then
					inp:ConnectTo(nil)
				elseif args.force or not inp:GetConnectedOutput() then
					local mod = args.modifier

					if not mod then
						local defmod =
						{
							Point = "Path",
						}

						mod = defmod[inp:GetAttrs("INPS_DataType")] or "BezierSpline"
					end

					if mod then
						inp:ConnectTo(comp[mod])
					end
				end
			end
		end
	},
	select =
	{
		"\tselect [(add|remove)]",
		(ws * (Cg("add", "add") + Cg("remove", "remove")))^-1,
		function(tool, args)
			comp.CurrentFrame.FlowView:Select(tool, not args.remove)
		end,
		function(tools, args)
			if not (args.add or args.remove) then
				comp.CurrentFrame.FlowView:Select(nil)
			end
		end,
	},
	color =
	{
		"\tcolor [tile <color>] [text <color>] [fill <color>]",
		(ws * ((P"tile" * ws * Cg(clrcolor, "tile")) + (P"text" * ws * Cg(clrcolor, "text")) + (P"fill" * ws * Cg(clrcolor, "fill"))))^1,
		function(tool, args)
			if args.tile ~= nil then tool.TileColor = args.tile or nil end
			if args.text ~= nil then tool.TextColor = args.text or nil end
			if args.fill ~= nil then tool.FillColor = args.fill or nil end
		end,
	},
	render =
	{
		"\trender [step <value>]",
		(ws * ((P"step" * ws * Cg(number, "stepValue"))))^-1,
		function(tool, args)
			local stepBy = 1
			if args.stepValue ~= nil then
				stepBy = tonumber(args.stepValue)
			end

			local command = [=[
if comp then
	tool = comp:FindTool("]=] ..tostring(tool.Name) .. [=[")
	if tool then
		local hiQ = true
		local networkRender = false

		local startFrame = comp:GetAttrs().COMPN_RenderStartTime
		local endFrame = comp:GetAttrs().COMPN_RenderEndTime
		local stepBy = ]=] ..tostring(stepBy) .. [=[


		-- Generate the frame list
		local frameList = ""
		if stepBy ~= nil and (stepBy == 1 or stepBy == 0) then
			frameList = tostring(startFrame) .. '..' .. tostring(endFrame)
		else
			frameList = startFrame
			for i = startFrame + stepBy, endFrame, stepBy do
				frameList = tostring(frameList) .. ',' .. tostring(i)
			end
		end

		-- Render the frame list
		print("[Render] ]=] ..tostring(tool.Name) .. [=[ [Frames] ", frameList)
		comp:Render({Tool = tool, FrameRange = frameList, HiQ = hiQ, UseNetwork = networkRender, Wait = true})
	end
end
			]=]
			-- print("[Render] [Command] " .. command)
			comp:Execute(command)
		end
	},
	version =
	{
		"\tversion [(up|down|to <value>)]",
		(ws * (Cg("up", "up") + Cg("down", "down") + (P"to" * ws * Cg(rest, "value"))))^-1,
		function(tool, args)
			if args.value ~= nil then
				local command = [=[
-- Code based upon PingKing's VersionUp/VersionDown Lua scripts
tool = comp:FindTool("]=] ..tostring(tool.Name) .. [=[")

if (tool:GetAttrs().TOOLS_RegID == "Saver") or (tool:GetAttrs().TOOLS_RegID == "Loader") then
	-- Get Saver's output location
	output_old = tool.Clip[fu.TIME_UNDEFINED]
	if output_old == "" or output_old == nil then
		-- error("[Version To][Error] The filename is currently empty. This script can only edit the version number if an output filename has already been specified.")
		return
	end

	-- Update a Loader node's frame range
	if (tool:GetAttrs().TOOLS_RegID == "Loader") then
		old_globalIn = tool.GlobalIn[fu.TIME_UNDEFINED]
		old_globalOut = tool.GlobalOut[fu.TIME_UNDEFINED]
		old_ClipStart = tool.ClipTimeStart[fu.TIME_UNDEFINED]
		old_ClipEnd = tool.ClipTimeEnd[fu.TIME_UNDEFINED]
		oldLastFrame = tool.HoldLastFrame[fu.TIME_UNDEFINED]
	end
 
	-- Version token matching
	version_find = string.match(output_old, '[Vv]%d+')
	if version_find == nil then
		return
	end

	version_prefix = string.sub(version_find, string.find(version_find, '[Vv]'), string.find(version_find, '[Vv]'))
	version_old_number = string.match(version_find, '%d+')
	version_padding = string.len(version_old_number)
	version_new_number = tonumber(]=] .. (args.value) .. [=[)
	version_new_number_padded = string.format("%0" .. version_padding .. "d",version_new_number)
	
	if version_new_number >= 0 then
		output_new = string.gsub(output_old, version_prefix .. version_old_number, version_prefix .. string.format("%0" .. version_padding .. "d",version_new_number))
	else
		return
	end
 
	comp:StartUndo("Version To")
	tool:SetAttrs({TOOLB_PassThrough = true})
	tool.Clip = output_new
  
	if (tool:GetAttrs().TOOLS_RegID == "Loader") then
		tool.GlobalOut[fu.TIME_UNDEFINED] = old_globalOut
		tool.GlobalIn[fu.TIME_UNDEFINED] = old_globalIn
		tool.ClipTimeEnd[fu.TIME_UNDEFINED] = old_ClipEnd
		tool.ClipTimeStart[fu.TIME_UNDEFINED] = old_ClipStart
		tool.HoldLastFrame[fu.TIME_UNDEFINED] = oldLastFrame
	end

	tool:SetAttrs({TOOLB_PassThrough = false})
	comp:EndUndo(true)
 
	print("[Version To] " .. tostring(tool:GetAttrs().TOOLS_Name) .. " [Old Ver] " .. tostring(version_old_number).. " [New Ver] " .. tostring(version_new_number_padded) .." [Path] " .. tostring(output_new))
end
]=]
				-- print("[Version To] [Command] " .. command)
				comp:Execute(command)
			elseif args.down ~= nil then
				local command = [=[
-- Code based upon PingKing's VersionUp/VersionDown Lua scripts
tool = comp:FindTool("]=] ..tostring(tool.Name) .. [=[")

if (tool:GetAttrs().TOOLS_RegID == "Saver") or (tool:GetAttrs().TOOLS_RegID == "Loader") then
	-- Get Saver's output location
	output_old = tool.Clip[fu.TIME_UNDEFINED]
	if output_old == "" or output_old == nil then
		-- error("[Version Down][Error] The filename is currently empty. This script can only edit the version number if an output filename has already been specified.")
		return
	end

	-- Update a Loader node's frame range
	if (tool:GetAttrs().TOOLS_RegID == "Loader") then
		old_globalIn = tool.GlobalIn[fu.TIME_UNDEFINED]
		old_globalOut = tool.GlobalOut[fu.TIME_UNDEFINED]
		old_ClipStart = tool.ClipTimeStart[fu.TIME_UNDEFINED]
		old_ClipEnd = tool.ClipTimeEnd[fu.TIME_UNDEFINED]
		oldLastFrame = tool.HoldLastFrame[fu.TIME_UNDEFINED]
	end
 
	-- Version token matching
	version_find = string.match(output_old, '[Vv]%d+')
	if version_find == nil then
		return
	end

	version_prefix = string.sub(version_find, string.find(version_find, '[Vv]'), string.find(version_find, '[Vv]'))
	version_old_number = string.match(version_find, '%d+')
	version_padding = string.len(version_old_number)
	version_new_number = tonumber(version_old_number)-1
	version_new_number_padded = string.format("%0" .. version_padding .. "d",version_new_number)
	
	if version_new_number >= 0 then
		output_new = string.gsub(output_old, version_prefix .. version_old_number, version_prefix .. string.format("%0" .. version_padding .. "d",version_new_number))
	else
		return
	end
 
	comp:StartUndo("Version Down")
	tool:SetAttrs({TOOLB_PassThrough = true})
	tool.Clip = output_new
  
	if (tool:GetAttrs().TOOLS_RegID == "Loader") then
		tool.GlobalOut[fu.TIME_UNDEFINED] = old_globalOut
		tool.GlobalIn[fu.TIME_UNDEFINED] = old_globalIn
		tool.ClipTimeEnd[fu.TIME_UNDEFINED] = old_ClipEnd
		tool.ClipTimeStart[fu.TIME_UNDEFINED] = old_ClipStart
		tool.HoldLastFrame[fu.TIME_UNDEFINED] = oldLastFrame
	end

	tool:SetAttrs({TOOLB_PassThrough = false})
	comp:EndUndo(true)
 
	print("[Version Down] " .. tostring(tool:GetAttrs().TOOLS_Name) .. " [Old Ver] " .. tostring(version_old_number).. " [New Ver] " .. tostring(version_new_number_padded) .." [Path] " .. tostring(output_new))
end
]=]
				-- print("[Version Down] [Command] " .. command)
				comp:Execute(command)
			elseif args.up ~= nil then
				local command = [=[
-- Code based upon PingKing's VersionUp/VersionDown Lua scripts
tool = comp:FindTool("]=] ..tostring(tool.Name) .. [=[")

if (tool:GetAttrs().TOOLS_RegID == "Saver") or (tool:GetAttrs().TOOLS_RegID == "Loader") then
	-- Get Saver's output location
	output_old = tool.Clip[fu.TIME_UNDEFINED]
	if output_old == "" or output_old == nil then
		-- error("[Version Up][Error] The filename is currently empty. This script can only edit the version number if an output filename has already been specified.")
		return
	end

	-- Update a Loader node's frame range
	if (tool:GetAttrs().TOOLS_RegID == "Loader") then
		old_globalIn = tool.GlobalIn[fu.TIME_UNDEFINED]
		old_globalOut = tool.GlobalOut[fu.TIME_UNDEFINED]
		old_ClipStart = tool.ClipTimeStart[fu.TIME_UNDEFINED]
		old_ClipEnd = tool.ClipTimeEnd[fu.TIME_UNDEFINED]
		oldLastFrame = tool.HoldLastFrame[fu.TIME_UNDEFINED]
	end
 
	-- Version token matching
	version_find = string.match(output_old, '[Vv]%d+')
	if version_find == nil then
		return
	end

	version_prefix = string.sub(version_find, string.find(version_find, '[Vv]'), string.find(version_find, '[Vv]'))
	version_old_number = string.match(version_find, '%d+')
	version_padding = string.len(version_old_number)
	version_new_number = tonumber(version_old_number)+1
	version_new_number_padded = string.format("%0" .. version_padding .. "d",version_new_number)
	
	if version_new_number >= 0 then
		output_new = string.gsub(output_old, version_prefix .. version_old_number, version_prefix .. string.format("%0" .. version_padding .. "d",version_new_number))
	else
		return
	end
 
	comp:StartUndo("Version Up")
	tool:SetAttrs({TOOLB_PassThrough = true})
	tool.Clip = output_new
  
	if (tool:GetAttrs().TOOLS_RegID == "Loader") then
		tool.GlobalOut[fu.TIME_UNDEFINED] = old_globalOut
		tool.GlobalIn[fu.TIME_UNDEFINED] = old_globalIn
		tool.ClipTimeEnd[fu.TIME_UNDEFINED] = old_ClipEnd
		tool.ClipTimeStart[fu.TIME_UNDEFINED] = old_ClipStart
		tool.HoldLastFrame[fu.TIME_UNDEFINED] = oldLastFrame
	end

	tool:SetAttrs({TOOLB_PassThrough = false})
	comp:EndUndo(true)
 
	print("[Version Up] " .. tostring(tool:GetAttrs().TOOLS_Name) .. " [Old Ver] " .. tostring(version_old_number).. " [New Ver] " .. tostring(version_new_number_padded) .." [Path] " .. tostring(output_new))
end
]=]
				-- print("[Version Up] [Command] " .. command)
				comp:Execute(command)
			end
		end
	},
}

local opts = parseopts(commands, args[0])

if opts then
	local tools = {}

	if opts.tooltype then
		for i,v in ipairs(opts.tooltype) do
			local tmp = comp:GetToolList(opts.scope == "selected", v)
			for i,v in ipairs(tmp) do
				table.insert(tools, v)
			end
		end
	else
		tools = comp:GetToolList(opts.scope == "selected", nil)
	end

	if opts.where then
		local old = bmd.getusing()
		local func,err = loadstring("return " .. opts.where)
		if not func then
			error(err)
		end

		local env = setmetatable({ time = comp.CurrentTime, tool = tool, input = inp }, { __index = function(t,k) return rawget(t,"tool"):GetInput(k, rawget(t,"time")) or _G[k] end })
		setfenv(func, env)

		local matchtools = {}

		for i,tool in ipairs(tools) do
			env.tool = tool
			local ok,ret = pcall(func)
			if ok and ret then
				table.insert(matchtools, tool)
			end
		end

		tools = matchtools
	end

	comp:StartUndo(args[0])

	for ic,v in ipairs(opts) do
		if commands[v.command][4] then
			commands[v.command][4](tools, v)
		end
		local func = commands[v.command][3]
		for it,tool in ipairs(tools) do
			if tool and opts.scope ~= "visible" or tool:GetAttrs("TOOLB_Visible") then
				ok,err = pcall(func, tool, v)
				if not ok then
					print(err)
				end
			end
		end
	end

	comp:EndUndo(true)
else
	print("Usage: /for (selected|visible|all) [tooltype[,tooltype...]] [where <condition>] <command> [ & <command>...]")
	print("Supported commands:")

	local t = {}

	for i,v in pairs(commands) do
		table.insert(t, v[1])
	end

	table.sort(t)

	print(table.concat(t,"\n"))
end
