# Caguas VP

## Virtual Production Cylinder Stitching ReadMe

By Robert Moreno and Andrew Hazelden  

## Overview

This Kartaverse stitching example shows an interesting workflow that batch stitches a PTGui Pro v12 .pts file inside of Fusion's node graph.

![Nighttime View of Caguas, Puerto Rico](images/stitched_cylinder.jpg)

The Kartaverse PT data nodes parse the .pts file to extract the location of the PTGui Pro batch rendered panoramic image.

![Stitching Nodes 1](images/pt_nodes_1.png)

The "ptLoader" node loads the external .pts document. The "ptBatchStitcher" node makes it possible to do inline stitching inside the nodegraph flow. The "ptOutputImage" node is used to load the stitched imagery into the composite.

![Stitching Nodes 2](images/pt_nodes_2.png)

Finally, rest of the example composite shows how to create a simulated virtual production LED video stage wall using Fusion's 3D workspace. The panoramic imagery is automatically placed onto this LED video wall surface.

A Camera3D node can then be flown around inside the virtual production stage filming volume to create previz versions of shots with the wrap-around live-action background plate visible.

![VP Nodegraph](images/comp_vp_previz.png)

## JSON Parsing in Fusion

PTGui v12 project files are stored in JSON format. This means both the Notepad++ for Fusion atom package, and the Kartaverse PT/Vonk Ultra nodes in Fusion can be used to read every single attribute on the fly.

![JSON in Notepad++ for Fusion](images/npp_for_fusion_json_pts_viewing.png)

## About the Footage

The "Nighttime View of Caguas, Puerto Rico (Dec 2021)" media was filmed by Robert Moreno. Robert's photographic process helped produced a cylindrical image projection output, with plenty of overlap to allow for precise artist-controlled blending and masking of each view.

This sample footage was captured using a Nikon D750 Camera with an AF DX Fisheye-Nikkor 10.5mm F/2.8 ED lens. A Nodal Ninja panoramic head was adjusted to an indexed rotation value of 15 degrees per view rotation increment, and 12 view angles were captured in the Nikon RAW NEF image format starting at 1:30 AM, local time on 2021-12-08.

Each photo was HDR exposure blended from a set of three RAW images taken at +3EV, 0EV, and -3EV. The pictures had an average of a 30 second exposure time, ISO 1600, aperture F/8, and the content was captured using a manual exposure mode. 

### Photographer Information

**North West Photography**  
Robert V Moreno Silva  
Puerto Rico, Caribbean  
+1 (939) 246-7702  

[nw360creator@gmail.com](mailto:nw360creator@gmail.com)  
[https://www.instagram.com/cave_manpr/](https://www.instagram.com/cave_manpr/)  

### Media Rights License

This Kartaverse example content is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) License.

Original Media Copyright © North West Photography, Dec 2021