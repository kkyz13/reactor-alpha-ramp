-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextFromCSV"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Fusion Text object by extracting a single cell from a CSV formatted block of text",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
GetHeading = [[
local vtextutils = self and require("vtextutils") or nil
tool = tool or comp.ActiveTool

local data = tool:GetInput("Input", comp.CurrentTime)
local row = 1
local col = tool:GetInput("Column")

local pattern = "(.-),"

-- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
local line = tostring(textutils.ReadLineIgnoreComments(data, row, "#")) .. ","

local array = textutils.split(line, pattern)
local array_size = table.getn(array)

local result = array[col]
if result ~= nil then
    comp:StartUndo("Rename Node")

    NewName = "csv_" .. tostring(result)
    tool:SetAttrs({TOOLS_Name = NewName})

    comp:EndUndo()
end
]]

    -- [[ Creates the user interface. ]]
    InRow= self:AddInput("Row", "Row", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 144,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MaxAllowed = 1000000,
        INP_Integer = true,
        LINK_Main = 2
    })
    InColumn= self:AddInput("Column", "Column", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 144,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MaxAllowed = 1000000,
        INP_Integer = true,
        LINK_Main = 3
    })
    InIgnoreHeaderRow = self:AddInput("Ignore Header Row", "IgnoreHeaderRow", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    InData = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 10,
        LINK_Main = 1
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 10,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_Passive = true,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    GetHeadingBtn = self:AddInput("Get Heading", "GetHeadingBtn", {
        INPID_InputControl = 'ButtonControl',
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetHeading,
        ICD_Width = 1.0,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InRow:SetAttrs({LINK_Visible = visible})
        InColumn:SetAttrs({LINK_Visible = visible})
        -- InData:SetAttrs({LINK_Visible = visible})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InData:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InData:SetAttrs({IC_Visible = false})
        InData:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InData:SetAttrs({TEC_Lines = lines})
    
        -- Toggle the visibility to refresh the inspector view
        InData:SetAttrs({IC_Visible = false})
        InData:SetAttrs({IC_Visible = true})
    end
end


function Process(req)
    -- [[ Creates the output. ]]
    local data = InData:GetValue(req).Value
    local header = tonumber(InIgnoreHeaderRow:GetValue(req).Value)
    local row = tonumber(InRow:GetValue(req).Value) + header
    local col = tonumber(InColumn:GetValue(req).Value)

    local pattern = "(.-),"

    -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
    local line = tostring(textutils.ReadLineIgnoreComments(data, row, "#")) .. ","

    local array = textutils.split(line, pattern)
    local array_size = table.getn(array)
    local result = array[col]

    -- print("[Data]\n", data)
    -- print("[Row]", row)
    -- print("[Column]", col)
    -- print("[Ignore Header Row]", header)
    -- print("[Line]", line)
    -- print("[Array]")
    -- dump(array)
    -- print("[Array Size]", array_size)
    -- print("[Result]", result)

    local out = Text(result)

    OutData:Set(req, out)
end
