-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextToHex"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Converts a string into Base16 Hex encoded text",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Input" , "Input" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 3,
        LINK_Main = 1
    })

    InSeparator = self:AddInput("Separator", "Separator", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        INPS_DefaultText = [[]],
        TEC_Lines = 1,
        LINK_Main = 2,
        INP_Required = false
    })

    InRemoveNonPrintableCharacters = self:AddInput("Remove Non-Printable Characters", "RemoveNonPrintableCharacters", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InText:SetAttrs({LINK_Visible = visible})
        InSeparator:SetAttrs({LINK_Visible = visible})
    end
end

function AsciiToHex(input, sep)
    return (input:gsub( ".", function(c)
        return string.format('%02X' .. sep, string.byte(c))
    end
    ))
end

function Process(req)
    -- [[ Creates the output. ]]
    local txt = InText:GetValue(req).Value

    -- Replace newlines and tabs: https://stackoverflow.com/questions/57099292/replace-n-string-with-real-n-in-lua
    local seperator = InSeparator:GetValue(req).Value:gsub("\\([nt])", {n = "\n", t = "\t", r = "\r"}) or ""

    -- Zap the gremlins found in invisible non-ASCII printable characters
    local remove = InRemoveNonPrintableCharacters:GetValue(req).Value
    if remove == 1 then
        -- txt = string.gsub(txt, "[%z\1-\31]", "")
        -- txt = string.gsub(txt, "[%W]", "")
        txt = string.gsub(txt, "[%z\1-\8\11-\12\14-\31]", "")
        txt = string.gsub(txt, "[\127-\255]", "")
    end

    local result = AsciiToHex(txt, seperator)

    local out = Text(result)

    OutText:Set(req, out)
end
