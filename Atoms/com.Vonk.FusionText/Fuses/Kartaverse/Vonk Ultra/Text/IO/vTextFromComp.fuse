-- Direct Fusion .comp file reading inspired by "AX Relativity.lua"
-- Match Example: "exr$", "jpg$", or "json$"
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextFromComp"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Read text strings from a Fusion .comp file.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    InFile = self:AddInput("File", "File", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 1
    })

   InMatch = self:AddInput("Match", "Match", {
       LINKID_DataType = "Text",
       INPID_InputControl = "TextEditControl",
       INPS_DefaultText = "",
       TEC_Lines = 1,
   })

    InFileExists = self:AddInput("File Exists", "FileExists", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InExpandPathMaps = self:AddInput("Expand PathMaps", "ExpandPathMaps", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InRemoveDuplicates = self:AddInput("Remove Duplicates", "RemoveDuplicates", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end


function Process(req)
    -- [[ Creates the output. ]]
    local filename = InFile:GetValue(req).Value
    local exists = InFileExists:GetValue(req).Value
    local sort = InSort:GetValue(req).Value
    local match = InMatch:GetValue(req).Value
    local remove_dup = InRemoveDuplicates:GetValue(req).Value

    local expandPathMaps = InExpandPathMaps:GetValue(req).Value

    -- Get the folder path to use later on for Comp:/ replacements
    local basepath = string.match(filename, "^(.+[/\\])")

    local out = ""

    fp = io.open(self.Comp:MapPath(filename), "r")
    if fp then
        local lines = {}
        for line in fp:lines() do
            table.insert(lines, line)
        end

        local paths = {}

        for i, v in ipairs(lines) do
            -- Look for a string in quotes (any text entry basically)
            findPattern = "%\"(.-)%\""

            s, e = v:find(findPattern)
            if s ~= nil then
                -- Remove the quotation marks
                cleanLine = string.gsub(v:sub(s,e), "%\"", "") 

                -- Verify there are forward or black slashes and there are no REGS help HTTP URLs
                --if cleanLine:find("[\\/]") ~= nil and not cleanLine:find("http") then
                if cleanLine:find("[\\/]") ~= nil then
                    -- Unescape the slashes
                    filepath = string.gsub(cleanLine, "\\\\", "\\")

                    -- Replace the per-document Comp: Pathmap
                    expandedFilepath = string.gsub(filepath, "^[Cc][Oo][Mm][Pp]:", basepath)

                    if exists == 1.0 then
                        -- Only add the path if the file exists on disk
                        if bmd.fileexists(self.Comp:MapPath(expandedFilepath)) then
                            -- Make the filepath absolute
                            if expandPathMaps == 1.0 then
                                filepath = self.Comp:MapPath(expandedFilepath)
                            end

                            -- Check if Lua pattern matching should be done
                            if match == "" or not match then
                                table.insert(paths, filepath)
                            elseif string.match(filepath, match) then
                                table.insert(paths, filepath)
                            end
                        end
                    else
                        -- Add the path even if the file doesn't exist on disk

                        -- Make the filepath absolute
                        if expandPathMaps == 1.0 then
                            filepath = self.Comp:MapPath(expandedFilepath)
                        end

                        -- Check if Lua pattern matching should be done
                        if match == "" or not match then
                            table.insert(paths, filepath)
                        elseif string.match(filepath, match) then
                            table.insert(paths, filepath)
                        end
                    end
                end
            end
        end

        fp:close()

        -- De-duplicate list items
        local result = {}
        if remove_dup == 1.0 then
            -- https://stackoverflow.com/questions/20066835/lua-remove-duplicate-elements/20067270#20067270
            local hash = {}
            for _,v in ipairs(paths) do
               if not hash[v] then
                   result[#result + 1] = v
                   hash[v] = true
               end
            end
        else
            result = paths
        end

        -- Sort the list alphabetically
        if sort == 1.0 then
            table.sort(result)
        end

        out = table.concat(result, "\n")
    else
        error(string.format("[vTextFromComp] Unable to read text from file '%s'", filename))
    end

    -- print("[Comp Paths]")
    -- dump(out)

    OutText:Set(req, Text(out))
end
