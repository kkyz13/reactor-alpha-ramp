-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vNumberCompFrameformat"
DATATYPE = "Number"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Number\\Comp",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns the comp's Frame Format.",
    REGS_OpIconString = FUSE_NAME,
    -- REG_TimeVariant = true, -- required to disable caching of the current time parameter
    -- REGB_Temporal = true, -- ensures reliability in Resolve 15
    REGS_IconID = "Icons.Tools.Icons.LightTrim",
})

function Create()
    -- [[ Creates the user interface. ]]
    InWidth = self:AddInput("Width", "Width", {
        LINKID_DataType = "Number",
        INP_SendRequest = false,
        INP_Required = false,
        INPID_InputControl = "SliderControl",
        INP_DelayDefault = true,
        LINK_ForceSave = true,
        INP_Passive = true,
        -- INP_Disabled = true,
        -- LINK_Main = 1
    })
    
    InHeight = self:AddInput("Height", "Height", {
        LINKID_DataType = "Number",
        INP_SendRequest = false,
        INP_Required = false,
        INPID_InputControl = "SliderControl",
        INP_DelayDefault = true,
        LINK_ForceSave = true,
        INP_Passive = true,
        -- INP_Disabled = true,
        -- LINK_Main = 2
    })

    OutWidth = self:AddOutput("Width", "Width", {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })
        
    OutHeight = self:AddOutput("Height", "Height", {
        LINKID_DataType = "Number",
        LINK_Main = 2
    })
end

function OnAddToFlow()
    -- read default size from comp preferences
    local formatPrefs = self.Comp:GetPrefs("Comp.FrameFormat")
    -- InWidth:SetAttrs({INP_Disabled = false,})
    InWidth:SetAttrs({INP_Default = formatPrefs.Width})
    -- InWidth:SetAttrs({INP_Disabled = true,})
    InHeight:SetAttrs({INP_Default = formatPrefs.Height})
end

function Process(req)
    -- [[ Creates the output. ]]
    local formatPrefs = self.Comp:GetPrefs("Comp.FrameFormat")
    local width = Number( formatPrefs.Width )
    local height = Number( formatPrefs.Height )

    InWidth:SetSource(width, 0)
    InWidth:SetAttrs({INP_Default = formatPrefs.Width})

    InHeight:SetSource(height, 0)
    InHeight:SetAttrs({INP_Default = formatPrefs.Height})

    OutWidth:Set(req, width)
    OutHeight:Set(req, height)
end

