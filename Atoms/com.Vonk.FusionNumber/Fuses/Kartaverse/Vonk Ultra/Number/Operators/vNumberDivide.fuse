-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vNumberDivide"
DATATYPE = "Number"
BTN_WIDTH = 0.33333

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Number\\Operators",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns the quotient of two numbers.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.LightTrim",
})

function Create()
    -- [[ Creates the user interface. ]]
    InNumerator = self:AddInput("Numerator" , "Numerator" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 1,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0
    })

    InNumeratorSeparator = self:AddInput("NumeratorSeparator" , "NumeratorSeparator" , {
        INPID_InputControl = "SeparatorControl",
        IC_Visible = true,
        INP_External = false,
    })

    InDenominator = self:AddInput("Denominator" , "Denominator" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 2,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutNumber = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]

    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then
            visible = true
        else
            visible = false
        end

        InNumerator:SetAttrs({LINK_Visible = visible})
        InDenominator:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local n1 = InNumerator:GetValue(req).Value
    local n2 = InDenominator:GetValue(req).Value

    -- print(n1)

    local result = n1 / n2
    local out = Number(result)

    OutNumber:Set(req, out)
end
