-- Based upon Fusion's built-in "Set Metadata.fuse"

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValToMetadata"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Meta",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Fusion image with metadata added from a ScriptVal object.",
    REGS_OpIconString = FUSE_NAME,
    REG_NoMotionBlurCtrls = true,
    REG_NoBlendCtrls = true,
    REG_OpNoMask = true,
    REG_NoPreCalcProcess = true, -- make default PreCalcProcess() behaviour be to call Process() rather than automatic pass through.
    REG_SupportsDoD = true,
    REG_Fuse_NoJIT = true,
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })

    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 2
    })

    InKey = self:AddInput("Key", "Key", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 3,
    })

    OutImage = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        -- InImage:SetAttrs({LINK_Visible = visible})
        InKey:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    local img = InImage:GetValue(req)
    local tbl = InScriptVal:GetValue(req):GetValue()
    local meta_key = InKey:GetValue(req).Value

    local result = Image({IMG_Like = img, IMG_NoData = req:IsPreCalc()})
    -- Crop (with no offset, ie. Copy) handles images having no data, so we don't need to put this within if/then/end
    img:Crop(result, {})

    -- Get value from metadata
    local meta_value
    if meta_key and meta_key ~= "" then
        -- Output the ScriptVal table as the image metadata record using the key name
        result.Metadata[meta_key] = tbl
    else
        -- Output the ScriptVal table as the image metadata record
        result.Metadata = tbl
    end


    OutImage:Set(req, result)
end
