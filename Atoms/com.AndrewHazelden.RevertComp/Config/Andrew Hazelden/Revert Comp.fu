--[[--
Revert Comp - v1 2023-09-25
By Andrew Hazelden <andrew@andrewhazelden.com>

## Overview ##

The "File > Revert Comp" menu closes the active Fusion Studio comp without saving it, then re-opens the comp again. This allows you to quickly do an experiment in Fusion, then flip back to the earlier document state.

Note: This menu item will not ask you to save the currently open composite document when closing it. If it did, then you wouldn't be able to revert to a prior saved version.

## Installation ##

Step 1. Move the "Revert Comp.fu" file into the Fusion user prefs "Config:/" folder.

Step 2. Restart Fusion so the config file is loaded.

--]]--

{
	Action
	{
		ID = "RevertComp",
		Category = "File",
		Name = "Revert Comp",

		Targets =
		{
			Composition =
			{
				Execute = _Lua [=[
-- Get the active comp filename
sourceComp = app:MapPath(obj:Comp():GetAttrs().COMPS_FileName)

-- Reopen the comp
if sourceComp ~= nil or sourceComp ~= '' then
	print('[Revert Comp] ', sourceComp)

	-- Open the comp after a 2 second delay
	codeStr = "bmd.wait(2);app:LoadComp([[" .. sourceComp .. "]], false, false, false)"
	-- print(codeStr)
	app:Execute(codeStr)

	-- If the comp is locked, it will not ask if the comp should be saved before closing.
	-- obj:Comp():Unlock()
	obj:Comp():Lock()

	-- Close the active comp
	obj:Comp():Close()
else
	print('[Revert Comp] Please save the untitled comp to disk first.')
end
				]=],
			},
		},
	},

	--Fusion 9 menus with three period characters
	Menus
	{
		Target = "ChildFrame",

		After "File\\Save a Copy As..."
		{
			"RevertComp{}",
		},
	},
	-- Fusion 16 menus with a Unicode elipsis character
	Menus
	{
		Target = "ChildFrame",

		After "File\\Save a Copy As…"
		{
			"RevertComp{}",
		},
	},
}
