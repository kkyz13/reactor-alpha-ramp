-- Based upon Fusion's built-in "Set Metadata.fuse"

-- ============================================================================
-- modules
-- ============================================================================
local base64utils = self and require("vbase64utils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vMetadataFromComp"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Meta\\Comp",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Fusion image with metadata added from a Fusion Studio comp.",
    REGS_OpIconString = FUSE_NAME,
    REG_NoMotionBlurCtrls = true,
    REG_NoBlendCtrls = true,
    REG_OpNoMask = true,
    REG_NoPreCalcProcess = true, -- make default PreCalcProcess() behaviour be to call Process() rather than automatic pass through.
    REG_SupportsDoD = true,
    REG_Fuse_NoJIT = true,
    REGS_IconID = "Icons.Tools.Icons.StickyNote",
})

function Create()
    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })

    InBase64 = self:AddInput("Base64 Encode", "Base64", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_DoNotifyChanged = true,
        INP_Integer = true,
        INP_Default = 0,
        ICD_Width = 1,
    })

--    InShowInput = self:AddInput("Show Input", "ShowInput", {
--        LINKID_DataType = "Number",
--        INPID_InputControl = "CheckboxControl",
--        INP_Integer = true,
--        INP_Default = 1.0,
--        INP_External = false,
--        INP_DoNotifyChanged = true
--    })

    OutImage = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
end

function read_file(filename)
    local f = assert(io.open(filename , "rb"))
    local content = f:read("*all")

    f:close()

    return content
end

function Process(req)
    local img = InImage:GetValue(req)
    local b64encode = InBase64:GetValue(req).Value

    local result = Image({IMG_Like = img, IMG_NoData = req:IsPreCalc()})
    -- Crop (with no offset, ie. Copy) handles images having no data, so we don't need to put this within if/then/end
    img:Crop(result, {})

    -- Get the current Fusion Studio/Fusion Render node based .comp file name
    -- Note: At this time this approach is not relevant for the Resolve Fusion page
    local rel_path = self.Comp.Filename
    local abs_path = self.Comp:MapPath(rel_path)

    if (rel_path ~= "") then
        local newmetadata = result.Metadata or {}

        -- Output the ScriptVal table as the image metadata record
        if b64encode == 1.0 then
             -- Base64 Encoded Comp
            local bin_data = base64utils.read_file(abs_path, "rb")
            local base64_str = base64utils.base64encode(bin_data)
            newmetadata.CompB64 = base64_str
        else
            -- Lua Table Encoded Comp
            local txt_str = read_file(abs_path)
            local tbl = bmd.readstring(txt_str)
            newmetadata.Comp = tbl
        end
        result.Metadata = newmetadata
    end

    OutImage:Set(req, result)
end
