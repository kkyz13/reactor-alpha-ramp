-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vPointFromText"
DATATYPE = "Point"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "Text",
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Point\\Text",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns a Fusion Point object from two Text inputs.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.Tracker",
})

function Create()
    -- [[ Creates the user interface. ]]
    InTextX = self:AddInput("InputX", "InputX", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Wrap = false,
        TEC_Lines = 1,
        LINK_Main = 1,
    })

    InTextY = self:AddInput("InputY", "InputY", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Wrap = false,
        TEC_Lines = 1,
        LINK_Main = 2,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutPoint = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InTextX:SetAttrs({LINK_Visible = visible})
        InTextY:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local numX = tonumber(InTextX:GetValue(req).Value)
    local numY = tonumber(InTextY:GetValue(req).Value)

    local point = Point(numX, numY)

    OutPoint:Set(req, point)
end
