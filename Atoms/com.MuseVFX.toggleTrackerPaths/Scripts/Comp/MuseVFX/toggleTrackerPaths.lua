print("Toggle Tracker Paths")

DEBUG = false

-- ===========================================================================
-- globals
-- ===========================================================================
_fusion = nil
_comp = nil
ui = nil
disp = nil



--==============================================================
-- Main()
--
-- Typical set-up for a script developed in an IDE. Sets globals
-- and connects to the active comp. Initializes UI Manger.
--==============================================================

function main()
	-- get fusion instance
	_fusion = getFusion()

	-- ensure a fusion instance was retrieved
	if not _fusion then
		error("Please open the Fusion GUI before running this tool.")
	end

	-- Set up aliases to the UI Manager framework
	ui = _fusion.UIManager
	disp = bmd.UIDispatcher(ui)
	
	-- get composition
	_comp = _fusion.CurrentComp
	SetActiveComp(_comp)

	-- ensure a composition is active
	if not _comp then
		error("Please open a composition before running this tool.")
	end

	dprint("\nActive comp is ".._comp:GetAttrs().COMPS_Name)

	_comp:StartUndo("Hide Tracker Paths")

	local trackerPathsHidden = _comp:GetData("TrackerPathsHidden")

	if trackerPathsHidden == true then
		dprint("Showing paths")
		_comp:SetData("TrackerPathsHidden", false)
	else
		dprint("Hiding paths")
		_comp:SetData("TrackerPathsHidden", true)
		trackerPathsHidden = false
	end

	--get a list of paths
	local toollist = _comp:GetToolList(false, "PolyPath")
	local pathlist = {}

	for i, tool in ipairs(toollist) do
		local _, _, name = string.find(tool.Name, "(%a+)")
		if name == 'Tracker' then
			dprint(tool.Name)
			table.insert(pathlist, tool)
		end
		if name == 'Path' then
			dprint(tool.Name)
			table.insert(pathlist, tool)
		end
	end

	for i, path in ipairs(pathlist) do 
		path.PolyLine:SetAttrs({ INPB_PC_Visible = trackerPathsHidden })
	end

	_comp:EndUndo(true)

end


------------------------------------------------------------------------
-- getFusion()
--
-- check if global fusion is set, meaning this script is being
-- executed from within fusion
--
-- Arguments: None
-- Returns: handle to the Fusion instance
------------------------------------------------------------------------
function getFusion()
	if fusion == nil then 
		-- remotely get the fusion ui instance
		fusion = bmd.scriptapp("Fusion", "localhost")
	end
	return fusion
end -- end of getFusion()


--========================== DEBUGGING ============================--


---------------------------------------------------------------------
-- dprint(string, suppressNewline)
--
-- Prints debugging information to the console when DEBUG flag is set
--
-- Arguments:
--		string, string, a message for the console
--		suppressNewline, boolean, do not start a new line
---------------------------------------------------------------------
function dprint(string, suppressNewline)
	local newline = "\n"
	if suppressNewline then newline = '' end
	if DEBUG then _comp:Print(string..newline) end
end -- dprint()

---------------------------------------------------------------------
-- ddump(object)
--
-- Performs a dump() if the DEBUG flag is set
--
-- Arguments
--		object, object, an object to be dumped
---------------------------------------------------------------------
function ddump(object)
	if DEBUG then dump(object) end
end -- end ddump()




main()