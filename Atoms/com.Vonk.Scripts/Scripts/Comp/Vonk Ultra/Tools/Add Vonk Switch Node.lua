--[[--
Add Vonk Switch Node

Inserts a switch node based upon the currently selected node's main output type.

Todo: 
Add a UI manager based dialog with:
- "Connection Order" comboMenu: Alphabetical, X-Axis, Y-Axis
- "Build Direction" comboMenu: Left/Right/Up/Down
--]]--

-- Read the selection
local tools = comp:GetToolList(true)
local selectedTool = tool or comp.ActiveTool or tools and tools[1]
if selectedTool then
	-- Check the selected node's output type
	toolOutput = selectedTool:FindMainOutput(1)
	if toolOutput ~= nil then
		toolType = toolOutput:GetAttrs().OUTS_DataType

		-- Lock the comp flow area
		comp:Lock()

		-- Add the correct switch node
		local switch
		if toolType == "Image" then
			switch = comp:AddTool("Fuse.vImageSwitch", -32768, -32768)
		elseif toolType == "Number" then
			switch = comp:AddTool("Fuse.vNumberSwitch", -32768, -32768)
		elseif toolType == "Text" then
			switch = comp:AddTool("Fuse.vTextSwitch", -32768, -32768)
		elseif toolType == "ScriptVal" then
			switch = comp:AddTool("Fuse.vScriptValSwitch", -32768, -32768)
		elseif toolType == "Point" then
			switch = comp:AddTool("Fuse.vPointSwitch", -32768, -32768)
		elseif toolType == "DataType3D" then
			switch = comp:AddTool("Fuse.v3DSwitch", -32768, -32768)
		end

		-- Connect the inputs
		for k,v in pairs(tools) do
			switch:ConnectInput("Input" .. k, tools[k])
		end

		-- Unlock the comp flow area
		comp:Unlock()
	end
end

-- End of the script
print('[Done]')
