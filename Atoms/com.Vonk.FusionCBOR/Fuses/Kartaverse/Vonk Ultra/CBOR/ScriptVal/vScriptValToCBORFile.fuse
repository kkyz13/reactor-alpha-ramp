-- ============================================================================
-- modules
-- ============================================================================
local cbor = self and require("cbor") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValToCBORFile"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = "ScriptVal",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\CBOR\\ScriptVal",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Writes a Fusion ScriptVal object to a CBOR binary file.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })

    InFile = self:AddInput("File", "File" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = true,
        FC_ClipBrowse = false,
        LINK_Visible = false,
        FCS_FilterString =  "Any Filetype (*.*)|*.*|",
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end


function write_file(path, content)
    --[[
        Writes content into a file.

        :param path: Path to write file to.
        :type path: string

        :param content: Content to write into a file.
        :type content: string
    ]]
    local directory = path:match("(.*[/\\])")

    if bmd.fileexists(directory) == false then
        bmd.createdir(directory)
        -- print('[Created Directory]', folder)
    end

    fp = io.open(path, "wb")
    if fp == nil then
        error(string.format("[Write Error] File or directory does not exist: %s", directory))
    end
    fp:write(content)
    fp:close()
end


function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()

--    print("[ScriptVal Lua Table]")
--    dump(txt_str)

    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local binary_data = cbor.encode(tbl)
    write_file(abs_path, binary_data)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
