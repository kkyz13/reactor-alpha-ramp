--[[--
Resolve Loader DragDrop - v1 2023-08-10 06.52 PM
By Andrew Hazelden <andrew@andrewhazelden.com>

Overview
----------
The "DragDrop" file allows you to bypass using MediaIn nodes when importing images into the Resolve Fusion page Nodes view from a desktop Explorer/Finder/Linux folder browsing window. The DragDrop Loader script is a quick way to bring content directly into your Resolve/Fusion composite. The DragDrop file supports dragging in multiple images at the same time, and each item will be imported into a separate Loader node.

Usage
------
Step 1. After you install the "Resolve Loader DragDrop" package, you will need to restart the Fusion program once so the new .fu file is loaded during Fusion's startup phase.

Step 2. Select an image by dragging it into the Nodes view from a desktop Explorer/Finder/Linux folder browsing window. The content will be automatically imported into the active Fusion composite as a Loader node.


1-Click Installation
---------------------
Install the "Resolve Loader DragDrop" atom package via the Reactor package manager.

This will install the "Resolve Loader DragDrop.fu" file into the "Config:/DragDrop/" PathMap folder (Reactor:/Deploy/Config/DragDrop/).


Fusion Standalone Manual "Config:/" Install:
	On Windows this PathMap works out to:
		%AppData%\Blackmagic Design\Fusion\Config\DragDrop\

	On Linux this PathMap works out to:
		$HOME/.fusion/BlackmagicDesign/Fusion/Config/DragDrop/

	On MacOS this works out to:
		$HOME/Library/Application Support/Blackmagic Design/Fusion/Config/DragDrop/

*Note: In a manual install of this tool you will have to create the final "DragDrop" folder manually as it won't exist in advance.

Resolve Fusion Page Manual "Config:/" Install:
	On Windows this PathMap works out to:
		%AppData%\Blackmagic Design\DaVinci Resolve\Support\Fusion\Config\DragDrop\

	On Linux this PathMap works out to:
		$HOME/.fusion/BlackmagicDesign/DaVinci Resolve/Support/Fusion/Config/DragDrop/

	On MacOS this PathMap works out to:
		$HOME/Library/Application Support/Blackmagic Design/DaVinci Resolve/Fusion/Config/DragDrop/

*Note: In a manual install of this tool you will have to create the final "DragDrop" folder manually as it won't exist in advance.


Todo
------
- Figure out the "args.ShiftModifier" equivalent variable name for detecting hotkeys from .fu events.

--]]--

{
	Event{
		Action = 'Drag_Drop',
		Targets = {
			FuView = {
				Execute = _Lua [=[
-- Check if the file extension matches
-- Example: isIFL = MatchExt('/Example.exr', '.exr')
function MatchExt(file, fileType)
	-- Get the file extension
	local ext = string.match(tostring(file), '^.+(%..+)$')

	-- Compare the results
	if (ext ~= nil) and (fileType ~= nil) and (string.lower(ext) == string.lower(tostring(fileType))) then
		return true
	else
		return false
	end
end


-- Get the current comp object
-- Example: comp = GetCompObject()
function GetCompObject()
	local cmp = app:GetAttrs().FUSIONH_CurrentComp
	return cmp
end



-- Process a file dropped into Fusion
-- Example: ProcessFile('/Example.exr', 1)
function ProcessFile(file, fileNum)
	print('[DragDrop][Loader]['.. fileNum .. '][File Drop] ', file, '\n')

	-- Check if the file extension matches
	if MatchExt(file, '.exr') or MatchExt(file, '.sxr') or MatchExt(file, '.jpg') or MatchExt(file, '.jpeg') or MatchExt(file, '.tiff') or MatchExt(file, '.tif') or MatchExt(file, '.png') or MatchExt(file, '.tga') or MatchExt(file, '.bmp') or MatchExt(file, '.dib') or MatchExt(file, '.ipl') or MatchExt(file, '.raw') or MatchExt(file, '.psd') then
		-- Accept the Drag and Drop event
		rets.handled = true

		-- Get the current comp object
		comp = GetCompObject()
		if not comp then
			-- The comp pointer is undefined
			print('[DragDrop][Loader] Please open a Fusion composite before dragging in an image filetype again.')
			return
		end

		---- Todo: Get the shift hotkey modifier working
		--if args.shiftModifier then
		-- Check for a shift key press to enable the alternative exr import option
		-- ...
		--end

		-- Lock the comp to suppress any file dialog opening for nodes that have empty filename fields.
		-- print('[Locking Comp]')
		comp:Lock()

		-- Disable the file browser dialog
		local AutoClipBrowse = app:GetPrefs('Global.UserInterface.AutoClipBrowse')
		app:SetPrefs('Global.UserInterface.AutoClipBrowse', false)

		-- Add the Loader node to the comp
		local ld = comp:AddTool('Loader', -32768, -32768)

		-- Todo: Add bmd.parseFilename() code here to start tokenizing the filename string and frame number element

		-- Check for a nil on the node creation and the filename string
		if ld and file then
			ld.Clip[fu.TIME_UNDEFINED] = tostring(file)
		else
			print('[DragDrop][Loader] Warning: Failed to update the Loader node filename.')
		end

		-- Re-enable the file browser dialog
		app:SetPrefs('Global.UserInterface.AutoClipBrowse', AutoClipBrowse)

		-- Unlock the comp to restore "normal" file dialog opening operations
		-- print('[Unlock Comp]')
		comp:Unlock()
	end
end


-- Where the magic begins
function Main()
	-- Call other chained events and default action
	rets = self:Default(ctx, args)

	-- Debug print the args
	-- dump(args)

	-- Drop zone screen coordinates
	mousex = args._sxpos
	mousey = args._sypos

	-- Check if no one else handled this, and we are running Resolve as the host app?
	if not rets.handled and app:GetVersion().App == "Resolve" then
		-- Get the list of files dropped into the Fusion page
		files = args.urilist

		-- Scan through each of the files
		for i, file in ipairs(files) do
			-- Process any image filetype dropped into the nodes view
			ProcessFile(file, i)
		end
	end

	-- Debug print where the file was dropped onscreen in the window (using screen coordinates)
	-- print('[Drop Zone Coords] [X] ' .. tostring(mousex) .. ' [Y] ' .. tostring(mousey) .. 'px')
	-- print('\n')
end

-- Run the main function
Main()
]=],
			},
		},
	},
}
