-- ============================================================================
-- modules
-- ============================================================================
--local json = self and require("dkjson") or nil
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArraySlice"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Array\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates Text from an array.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]
    InData = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })

    -- InKey = self:AddInput("Key", "Key", {
        -- LINKID_DataType = "Text",
        -- INPID_InputControl = "TextEditControl",
        -- TEC_Lines = 1,
        -- LINK_Main = 2
    -- })
    
    
    InStart = self:AddInput("Start", "Start", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_Default = 1,
        INP_Integer = true,
        INP_MinScale = 1,
        INP_MaxScale = 4,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MaxAllowed = 1000000,
        LINK_Main = 2
    })
    
    InStop = self:AddInput("Stop", "Stop", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_Default = 1,
        INP_Integer = true,
        INP_MinScale = 1,
        INP_MaxScale = 4,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MaxAllowed = 1000000,
        LINK_Main = 3
    })
    
    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutArray = self:AddOutput("Array", "Array", {
        LINKID_DataType = "Text",
        --INPID_InputControl = "TextEditControl",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InStart:SetAttrs({LINK_Visible = visible})
        InStop:SetAttrs({LINK_Visible = visible})
        InData:SetAttrs({LINK_Visible = visible})
    end
end

function Process( req )
    -- [[ Creates the output. ]]
    local json_string = InData:GetValue(req).Value
    local start = InStart:GetValue(req).Value
    local stop = InStop:GetValue(req).Value


    local lua_table = jsonutils.decode(json_string)
    local lua_array = arrayutils(lua_table["array"])
    
    
    local temp = lua_array:Slice(start, stop)
    --dump(temp)
    
    local array = {}
    
    array["array"] = temp
    array["size"] = table.getn(array["array"])
    --dump(array)

    local out = Text(jsonutils.encode(array))
    
    OutArray:Set(req, out)
end