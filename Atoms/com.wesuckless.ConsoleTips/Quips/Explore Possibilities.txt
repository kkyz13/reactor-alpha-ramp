{[[Don't go by name of the tool. Explore the possibilities. A single background tool can be used to create circle offsets, rectangle offsets, mirror offsets. A text+ tool with a follower modifier can be used to create all kinds of minimal background animation effects, burst effects, loading effects just by typing symbols instead of alphabets in the styled text field.

-- Rjun Rajput]]}
