--[[--
----------------------------------------------------------------------
Copyright (c) 2022, Jacob Danell, Ember Light AB <jacob@emberlight.se>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
----------------------------------------------------------------------
version 1.0
2024-01-09:
--]]
--

local version = "1.0"
local authorText = "v" .. version .. " - Created by Jacob Danell, Ember Light"

FuRegisterClass("GroundCheck", CT_Modifier, {
	REGS_Name            = "Ground Check",
	REGS_Category        = "Ember Light",
	REGS_OpDescription   = "Get the ground position from an alpha channel",
	REGS_OpIconString    = "GC",
	REGS_Company         = "Ember Light",
	REGS_URL             = "https://www.emberlight.se",
	REGID_DataType       = "Point",
	REGID_InputDataType  = "Point",
	REGS_HelpTopic       = "",
	REG_Fuse_NoEdit      = false,
	REG_Fuse_NoReload    = false,
	REG_NoPreCalcProcess = true,
	REG_Version          = 100,
})


function Create()
	InImage = self:AddInput("Input", "Input", {
		LINKID_DataType = "Image",
		INPID_InputControl = "ImageControl",
		LINK_Main = 1,
	})

	InChannel = self:AddInput("Channel", "Channel", {
		LINKID_DataType    = "Number",
		INPID_InputControl = "ComboControl",
		INP_Default        = 3,
		INP_External       = false,
		{ CCS_AddString = "Red" },
		{ CCS_AddString = "Green" },
		{ CCS_AddString = "Blue" },
		{ CCS_AddString = "Alpha" },
	});

	InLocator = self:AddInput("Locator", "Locator", {
		LINKID_DataType = "Point",
		INPID_InputControl = "OffsetControl",
		INPID_PreviewControl = "CrosshairControl",
		LINK_Main = 2,
		INP_DoNotifyChanged = true,
	})

	InOffset = self:AddInput("Offset", "Offset", {
		LINKID_DataType      = "Point",
		INPID_InputControl   = "OffsetControl",
		INPID_PreviewControl = "CrosshairControl",
		CHC_Style            = "DiagonalCross",
		INP_DefaultX         = 0,
		INP_DefaultY         = 0,
		PC_GrabPriority      = -1,
	})

	InAngle = self:AddInput("Angle", "Angle", {
		LINKID_DataType      = "Number",
		INPID_InputControl   = "ScrewControl",
		INPID_PreviewControl = "AngleControl",
		ACP_Center           = InLocator,
		INP_MaxScale         = 50,
		INP_Default          = 90,
	})

	self:AddInput(" ", "sep1", {
		INPID_InputControl = "SeparatorControl",
	})

	InSearchDistance = self:AddInput("Search Distance", "SearchDistance", {
		LINKID_DataType    = "Number",
		INPID_InputControl = "SliderControl",
		INP_Default        = 25,
		INP_MinAllowed     = 1,
		INP_MaxAllowed     = 100,
	})

	InSearchSteps = self:AddInput("Search Steps", "SearchSteps", {
		LINKID_DataType    = "Number",
		INPID_InputControl = "SliderControl",
		INP_Default        = 20,
		INP_MinScale       = 1,
		INP_MinAllowed     = 1,
		INP_MaxScale       = 30,
		INP_Integer        = true,
	})

	self:AddInput("", "sepAuth", {
		INPID_InputControl = "SeparatorControl",
	})

	Author = self:AddInput(authorText, "Author", {
		LINKID_DataType    = "Text",
		INPID_InputControl = "LabelControl",
		INP_External       = false,
		INP_Passive        = true,
		IC_NoLabel         = true,
		IC_NoReset         = true,
		LBLC_LabelColor    = 1,
	})

	OutValue = self:AddOutput("Output", "Output", {
		LINKID_DataType = "Point",
		LINK_Main = 1,
	})
end

function NotifyChanged(inp, param, time)
	if inp ~= nil and param ~= nil then
		if inp == InLocator then
			-- the center moved, reposition the axis according to the current offset
			nCenterX = param.X
			nCenterY = param.Y

			InOffset:SetAttrs({
				PCD_OffsetX = nCenterX,
				PCD_OffsetY = nCenterY,
			})
		end
	end
end

function CheckRequest(req)
	if (req:GetPri() == 0) then
		req:ClearInputFlags(InImage, REQF_PreCalc)
	end
end

function GetAlpha(img, x, y, channel)
	if x > img.Width - 1 then
		x = img.Width - 1
	elseif x < 0 then
		x = 0
	end

	if y > img.Height - 1 then
		y = img.Height - 1
	elseif y < 0 then
		y = 0
	end

	local p = Pixel()
	img:GetPixel(x, y, p)
	if channel == 0 then
		return p.R
	elseif channel == 1 then
		return p.G
	elseif channel == 2 then
		return p.B
	else
		return p.A
	end
end

function Process(req)
	local location = InLocator:GetValue(req)
	local pos = Point(location.X, location.Y)
	local img = InImage:GetValue(req)
	local channel = InChannel:GetValue(req).Value

	local minSearch = 0
	local maxSearch = math.max(img.Height, img.Width) * (InSearchDistance:GetValue(req).Value * 0.01)
	local yLocation = location.Y * img.Height
	local xLocation = location.X * img.Width
	local i = 0
	local oldDelta = 0
	local hitTest = nil
	local delta = 0

	function calculateDirection(degrees)
		local x, y

		degrees = degrees % 360
		if degrees > 180 then
			x = 360 - degrees
		else
			x = degrees
		end
		x = -(x - 90) / 90

		degrees = (degrees + 90) % 360
		if degrees >= 180 then
			y = 360 - degrees
		else
			y = degrees
		end
		y = (y - 90) / 90

		return x, y
	end

	local xDir, yDir = calculateDirection(InAngle:GetValue(req).Value)

	if GetAlpha(img, xLocation, yLocation) >= 0.5 then
		while math.abs(minSearch - maxSearch) > 0.01 and i < InSearchSteps:GetValue(req).Value do
			-- The next step to serach on
			delta = (minSearch + (maxSearch - minSearch) * 0.5) - oldDelta
			oldDelta = minSearch + (maxSearch - minSearch) * 0.5

			xLocation = xLocation + (xDir * delta)
			yLocation = yLocation + (yDir * delta)

			hitTest = GetAlpha(img, xLocation, yLocation, channel)
			i = i + 1

			if hitTest > 0 then
				minSearch = oldDelta
			elseif hitTest <= 0 then
				maxSearch = oldDelta
			end
		end

		yLocation = math.floor(yLocation - 0.5) + GetAlpha(img, xLocation, yLocation - 0.5, channel)
	end
	local offset = InOffset:GetValue(req)
	pos = Point(xLocation / img.Width + (offset.X), yLocation / img.Height + (offset.Y))

	OutValue:Set(req, pos)
end
