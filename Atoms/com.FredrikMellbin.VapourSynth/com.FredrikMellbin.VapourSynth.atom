Atom {
	Name = "VapourSynth",
	Category = "Bin",
	Author = "Fredrik Mellbin",
	Version = 44,
	Date = {2018, 12, 24},
	Description = [[<h2>Overview</h2>

<p><a href="http://www.vapoursynth.com/">VapourSynth R44</a> is an application for video manipulation. Or a plugin. Or a library. or a Frameserver. It’s hard to tell because it has a core library written in C++ and a Python module to allow video scripts to be created.</p>

<h2>Open Source License</h2>
<p>VapourSynth is licensed under an MIT open-source license.<br>
The VapourSynth software uses Qt 5 framework by The Qt Company, distributed under LGPL license.<br>
Silk icons v1.3 by Mark James, distributed under Creative Commons Attribution 2.5 License.<br>
FatCow free icons, distributed under Creative Commons Attribution 3.0 License.</p>


<h2>For More Information</h2>

<p>Check out the project's GitHub page, and the main VapourSynth homepage for more learning resources</p>

<p><a href="https://github.com/vapoursynth/vapoursynth">https://github.com/vapoursynth/vapoursynth</a><br>
<a href="http://www.vapoursynth.com/">http://www.vapoursynth.com/</a><br>
<a href="http://www.vapoursynth.com/doc/">VapourSynth Documentation</a><br>
<a href="https://vsdb.top/">VSDB - VapourSynth Database</a></p>


<p>This atom package was prepared by <a href="mailto:andrew@andrewhazelden.com">Andrew Hazelden</a> based upon the work of <a href="https://zacharyhalberd.com/blog/2021/6/14/vapoursynth-install-package-for-python-368">Zachary Halberd's VapourSynth Installation for Python 3.6.8</a> release.



<h2>What's Included</h2>

<p>Programs:</p>

<ul>
	<li>Python 3.6.8 Portable</li>
	<li>VapourSynth Portable R44</li>
	<li>VapourSynthEditor R19</li>
</ul>


<p>Libraries:</p>

<ul>
	<li>ffms2 2.40 msvc</li>
	<li>fmtconv r22</li>
	<li>vapoursynth mvtools v23 win64</li>
	<li>vapoursynth nnedi3 v12 win64</li>
	<li>havsfunc r33</li>
	<li>mvsfunc r8</li>
	<li>vapoursynth adjust v1</li>
	<li>VapourSynth script master-v1</li>
</ul>
]],
	Deploy = {

		Windows = {
			"Bin/VapourSynth/AVFS.exe",
			"Bin/VapourSynth/CHANGELOG",
			"Bin/VapourSynth/D3Dcompiler_47.dll",
			"Bin/VapourSynth/LICENSE",
			"Bin/VapourSynth/MANIFEST.in",
			"Bin/VapourSynth/Qt5Core.dll",
			"Bin/VapourSynth/Qt5Gui.dll",
			"Bin/VapourSynth/Qt5Network.dll",
			"Bin/VapourSynth/Qt5Svg.dll",
			"Bin/VapourSynth/Qt5WebSockets.dll",
			"Bin/VapourSynth/Qt5Widgets.dll",
			"Bin/VapourSynth/Qt5WinExtras.dll",
			"Bin/VapourSynth/README",
			"Bin/VapourSynth/VSPipe.exe",
			"Bin/VapourSynth/VSScript.dll",
			"Bin/VapourSynth/VSVFW.dll",
			"Bin/VapourSynth/VapourSynth.dll",
			"Bin/VapourSynth/Vapoursynth_Requirements_20210614.txt",
			"Bin/VapourSynth/__pycache__/adjust.cpython-36.pyc",
			"Bin/VapourSynth/__pycache__/havsfunc.cpython-36.pyc",
			"Bin/VapourSynth/__pycache__/mvsfunc.cpython-36.pyc",
			"Bin/VapourSynth/_asyncio.pyd",
			"Bin/VapourSynth/_bz2.pyd",
			"Bin/VapourSynth/_ctypes.pyd",
			"Bin/VapourSynth/_decimal.pyd",
			"Bin/VapourSynth/_distutils_findvs.pyd",
			"Bin/VapourSynth/_elementtree.pyd",
			"Bin/VapourSynth/_hashlib.pyd",
			"Bin/VapourSynth/_lzma.pyd",
			"Bin/VapourSynth/_msi.pyd",
			"Bin/VapourSynth/_multiprocessing.pyd",
			"Bin/VapourSynth/_overlapped.pyd",
			"Bin/VapourSynth/_socket.pyd",
			"Bin/VapourSynth/_sqlite3.pyd",
			"Bin/VapourSynth/_ssl.pyd",
			"Bin/VapourSynth/adjust.py",
			"Bin/VapourSynth/bearer/qgenericbearer.dll",
			"Bin/VapourSynth/bearer/qnativewifibearer.dll",
			"Bin/VapourSynth/concrt140.dll",
			"Bin/VapourSynth/havsfunc.py",
			"Bin/VapourSynth/iconengines/qsvgicon.dll",
			"Bin/VapourSynth/imageformats/qdds.dll",
			"Bin/VapourSynth/imageformats/qgif.dll",
			"Bin/VapourSynth/imageformats/qicns.dll",
			"Bin/VapourSynth/imageformats/qico.dll",
			"Bin/VapourSynth/imageformats/qjpeg.dll",
			"Bin/VapourSynth/imageformats/qsvg.dll",
			"Bin/VapourSynth/imageformats/qtga.dll",
			"Bin/VapourSynth/imageformats/qtiff.dll",
			"Bin/VapourSynth/imageformats/qwbmp.dll",
			"Bin/VapourSynth/imageformats/qwebp.dll",
			"Bin/VapourSynth/libEGL.dll",
			"Bin/VapourSynth/libGLESV2.dll",
			"Bin/VapourSynth/msvcp140.dll",
			"Bin/VapourSynth/msvcp140_1.dll",
			"Bin/VapourSynth/msvcp140_2.dll",
			"Bin/VapourSynth/mvsfunc.py",
			"Bin/VapourSynth/nnedi3_resample.py",
			"Bin/VapourSynth/opengl32sw.dll",
			"Bin/VapourSynth/pfm-191-vapoursynth-win.exe",
			"Bin/VapourSynth/platforms/qwindows.dll",
			"Bin/VapourSynth/portable.vs",
			"Bin/VapourSynth/pyexpat.pyd",
			"Bin/VapourSynth/python.exe",
			"Bin/VapourSynth/python3.dll",
			"Bin/VapourSynth/python36._pth",
			"Bin/VapourSynth/python36.dll",
			"Bin/VapourSynth/python36.zip",
			"Bin/VapourSynth/pythonw.exe",
			"Bin/VapourSynth/run_bat.py",
			"Bin/VapourSynth/select.pyd",
			"Bin/VapourSynth/setup.py",
			"Bin/VapourSynth/sqlite3.dll",
			"Bin/VapourSynth/styles/qwindowsvistastyle.dll",
			"Bin/VapourSynth/test.bat",
			"Bin/VapourSynth/test1.vpy",
			"Bin/VapourSynth/unicodedata.pyd",
			"Bin/VapourSynth/vapoursynth.cp36-win_amd64.pyd",
			"Bin/VapourSynth/vapoursynth64/coreplugins/AvsCompat.dll",
			"Bin/VapourSynth/vapoursynth64/coreplugins/EEDI3.dll",
			"Bin/VapourSynth/vapoursynth64/coreplugins/MiscFilters.dll",
			"Bin/VapourSynth/vapoursynth64/coreplugins/Morpho.dll",
			"Bin/VapourSynth/vapoursynth64/coreplugins/RemoveGrainVS.dll",
			"Bin/VapourSynth/vapoursynth64/coreplugins/VIVTC.dll",
			"Bin/VapourSynth/vapoursynth64/coreplugins/Vinverse.dll",
			"Bin/VapourSynth/vapoursynth64/coreplugins/avisource.dll",
			"Bin/VapourSynth/vapoursynth64/coreplugins/libhistogram.dll",
			"Bin/VapourSynth/vapoursynth64/coreplugins/libsubtext.dll",
			"Bin/VapourSynth/vapoursynth64/plugins/ffms2.dll",
			"Bin/VapourSynth/vapoursynth64/plugins/fmtconv.dll",
			"Bin/VapourSynth/vapoursynth64/plugins/libmvtools.dll",
			"Bin/VapourSynth/vapoursynth64/plugins/libnnedi3.dll",
			"Bin/VapourSynth/vapoursynth64/plugins/nnedi3_weights.bin",
			"Bin/VapourSynth/vccorlib140.dll",
			"Bin/VapourSynth/vcruntime140.dll",
			"Bin/VapourSynth/vsedit-job-server-watcher.exe",
			"Bin/VapourSynth/vsedit-job-server.exe",
			"Bin/VapourSynth/vsedit.exe",
			"Bin/VapourSynth/vsedit.ico",
			"Bin/VapourSynth/vsedit.svg",
			"Bin/VapourSynth/winsound.pyd",
		},
	},
	Dependencies = {
	},
}
