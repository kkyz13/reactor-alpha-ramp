import sys
import os

# Windows Path
windows_path_base = app.MapPath("Reactor:/Deploy/Modules/PSDRexa/")
windows_user_path_base = app.MapPath("Reactor:/Deploy/Modules/PSDRexa/")

# Mac Path
mac_path_base = app.MapPath("Reactor:/Deploy/Modules/PSDRexa/")
mac_user_path_base = app.MapPath("Reactor:/Deploy/Modules/PSDRexa/")

# Selecting the path to use
if os.name == "nt":
    path_base = windows_path_base
    user_path_base = windows_user_path_base
else:
    path_base = mac_path_base
    user_path_base = mac_user_path_base

# Add path
sys.path.append(path_base)
sys.path.append(os.path.join(path_base, "module"))

# If a per-user installation path exists, also add that path.
if os.path.exists(user_path_base):
    sys.path.append(user_path_base)
    sys.path.append(os.path.join(user_path_base, "module"))

from PSDRexaSyncer.main_ui import PSDRexaSyncerUI


def ErrorWindow(status, message):
    ui = fu.UIManager
    error_disp = bmd.UIDispatcher(ui)

    error_dlg = error_disp.AddWindow(
        {
            "WindowTitle": "",
            "ID": "ErrorWin",
            "TargetID": "ErrorWin",
            "Geometry": [0, 85, 1400, 200],
            "Spacing": 0,
        },
        [
            ui.VGroup(
                {
                    "ID": "root",
                    "Weight": 10.0,
                },
                [
                    ui.TextEdit({"ID": "ErrorTxt", "Text": message, "Weight": 1.0}),
                    ui.VGap(10),
                    ui.Button(
                        {
                            "ID": "OKButton",
                            "Text": "OK",
                            "Weight": 0.01,
                        }
                    ),
                ],
            ),
        ],
    )

    error_itm = error_dlg.GetItems()

    # The window was closed
    def ErrorWinFunc(ev):
        error_disp.ExitLoop()

    error_dlg.On.ErrorWin.Close = ErrorWinFunc

    # Add your GUI element based event functions here:

    def OKButtonFunc(ev):
        error_disp.ExitLoop()

    error_dlg.On.OKButton.Clicked = OKButtonFunc

    error_dlg.Show()
    error_disp.RunLoop()
    error_dlg.Hide()


try:
    from psd_tools import psd
    from PIL import Image
except ModuleNotFoundError:
    ErrorWindow(
        "",
        "PSDRexa",
        """PSDRexa Python module is missing. The macOS & Linux Terminal based install commands for PSDRexa are:
pip3 install --upgrade pip
pip3 install PIL
pip3 install psd_tools
	""",
    )
    exit()

ui = PSDRexaSyncerUI(fu, bmd, resolve)
