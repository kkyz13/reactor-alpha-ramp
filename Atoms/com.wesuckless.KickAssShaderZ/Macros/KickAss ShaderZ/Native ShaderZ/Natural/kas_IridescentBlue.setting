{
	Tools = ordered() {
		kas_IridescentBlue = GroupOperator {
			CtrlWZoom = false,
			NameSet = true,
			CustomData = {
				Settings = {
				},
				HelpPage = "https://www.steakunderwater.com/wesuckless/viewtopic.php?p=25255#p25255"
			},
			Inputs = ordered() {
				Comments = Input { Value = "\"kas_IridescentBlue\" creates a luminous material with a saturated blue sheen, and a fresnel edge shading effect. \n\nCopyright Credits \nkas_Parkland.exr\n\nCreative Commons:\nEgg mountain at afternoon \n\nBy:\nhttps://hdrmaps.com \n\nIs licensed under:\nCC BY 2.0", },
				TextureMap = InstanceInput {
					SourceOp = "TextureInputSwitchElseFuse",
					Source = "Input1",
					Name = "TextureMap",
				},
				EnvironmentMap = InstanceInput {
					SourceOp = "EnvironmentMapInputSwitchElseFuse",
					Source = "Input1",
					Name = "EnvironmentMap",
				}
			},
			Outputs = {
				Material = InstanceOutput {
					SourceOp = "ShaderPipeRouter",
					Source = "Output",
					Name = "Material Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 660, 115.5 },
				Flags = {
					AllowPan = false,
					AutoSnap = true
				},
				Size = { 466.07, 260.114, 459.064, 21.3808 },
				Direction = "Horizontal",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { -1381.99, -285.673 }
			},
			Tools = ordered() {
				ParklandLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_Parkland.exr",
							FormatID = "OpenEXRFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						["Clip1.OpenEXRFormat.RedName"] = Input { Value = FuID { "R" }, },
						["Clip1.OpenEXRFormat.GreenName"] = Input { Value = FuID { "G" }, },
						["Clip1.OpenEXRFormat.BlueName"] = Input { Value = FuID { "B" }, },
						["Clip1.OpenEXRFormat.AlphaName"] = Input { Value = FuID { "A" }, },
						Comments = Input { Value = "Copyright Credits \nkas_Parkland.exr\n\nCreative Commons:\nEgg mountain at afternoon \n\nBy:\nhttps://hdrmaps.com \n\nIs licensed under:\nCC BY 2.0", },
					},
					ViewInfo = OperatorInfo { Pos = { 1209.33, 312.894 } },
				},
				AgedSteelPlateLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_AgedSteelPlate.jpg",
							FormatID = "JpegFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
					},
					ViewInfo = OperatorInfo { Pos = { 1045, 313.5 } },
				},
				EnvironmentMapInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "ParklandLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 346.5 } },
					Version = 100
				},
				TextureInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "AgedSteelPlateLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1045, 346.5 } },
					Version = 100
				},
				ShaderPipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "IridescentBlueReflect",
							Source = "MaterialOutput",
						},
					},
					ViewInfo = PipeRouterInfo { Pos = { 1320, 478.5 } },
				},
				IridescentBlueReflect = MtlReflect {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						BackgroundMaterial = Input {
							SourceOp = "BlueFalloff",
							Source = "MaterialOutput",
						},
						["Reflection.GlancingStrength"] = Input { Value = 0, },
						["Reflection.FaceOnStrength"] = Input { Value = 0.944, },
						["Reflection.Falloff"] = Input { Value = 2.545, },
						["Reflection.Color.Material"] = Input {
							SourceOp = "ParklandFalloff",
							Source = "MaterialOutput",
						},
						["Reflection.Intensity.Material"] = Input {
							SourceOp = "RedToAlphaChannelBooleans",
							Source = "Output",
						},
						["Refraction.RefractiveIndex.SeparateRGBIndices"] = Input { Value = 1, },
						["Refraction.RefractiveIndex.RGB"] = Input { Value = 0.537222222222222, },
						["Refraction.Tint.Red"] = Input { Value = 0, },
						["Refraction.Tint.Green"] = Input { Value = 0.418744527589964, },
						MaterialID = Input { Value = 82, },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 478.5 } },
				},
				ParklandFalloff = FalloffOperator {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["FaceOn.Red"] = Input { Value = 0.108, },
						["FaceOn.Green"] = Input { Value = 0.89296, },
						["FaceOn.Opacity"] = Input { Value = 1, },
						["Glancing.Red"] = Input { Value = 0.277, },
						["Glancing.Green"] = Input { Value = 0.39268, },
						["Glancing.Opacity"] = Input { Value = 0, },
						Falloff = Input { Value = 6.601, },
						FaceOnMaterial = Input {
							SourceOp = "ParklandSphereMap",
							Source = "MaterialOutput",
						},
						MaterialID = Input { Value = 7, },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 445.5 } },
				},
				ParklandSphereMap = SphereMap {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Rotation = Input { Value = 1, },
						["Rotate.Y"] = Input { Value = -65, },
						Image = Input {
							SourceOp = "ParklandBlur",
							Source = "Output",
						},
						MaterialID = Input { Value = 15, },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 412.5 } },
				},
				ParklandBlur = Blur {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input { Value = FuID { "Gaussian" }, },
						XBlurSize = Input { Value = 15, },
						Input = Input {
							SourceOp = "EnvironmentMapInputSwitchElseFuse",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 379.5 } },
				},
				BlueFalloff = FalloffOperator {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						ColorVariation = Input { Value = FuID { "Gradient" }, },
						["FaceOn.Opacity"] = Input { Value = 0.5105263, },
						["Glancing.Red"] = Input { Value = 0, },
						["Glancing.Green"] = Input { Value = 0, },
						["Glancing.Blue"] = Input { Value = 0, },
						["Glancing.Alpha"] = Input { Value = 0, },
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 0, 0.671689643756996, 1, 0 },
									[1] = { 0, 0.671689643756996, 1, 0 }
								}
							},
						},
						Falloff = Input { Value = 0.177, },
						FaceOnMaterial = Input {
							SourceOp = "AgedBlueWard",
							Source = "MaterialOutput",
						},
						MaterialID = Input { Value = 9, },
					},
					ViewInfo = OperatorInfo { Pos = { 1045, 479.462 } },
				},
				AgedBlueWard = MtlWard {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Diffuse.Color.Red"] = Input { Value = 0.13633013408531, },
						["Diffuse.Color.Green"] = Input { Value = 0.12, },
						["Diffuse.Color.Material"] = Input {
							SourceOp = "RedToAlphaChannelBooleans",
							Source = "Output",
						},
						["Diffuse.Opacity"] = Input { Value = 0.504531722054381, },
						["Specular.Nest"] = Input { Value = 1, },
						["Specular.Intensity"] = Input { Value = 0, },
						["Transmittance.Nest"] = Input { Value = 1, },
						MaterialID = Input { Value = 4, },
					},
					ViewInfo = OperatorInfo { Pos = { 1045, 445.5 } },
				},
				RedToAlphaChannelBooleans = ChannelBoolean {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						ToAlpha = Input { Value = 5, },
						Background = Input {
							SourceOp = "AgedBrightnessContrast",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1045.96, 412.5 } },
				},
				AgedBrightnessContrast = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Saturation = Input { Value = 0, },
						Low = Input { Value = 0.2985507, },
						High = Input { Value = 0.4985507, },
						Input = Input {
							SourceOp = "TextureInputSwitchElseFuse",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1045, 379.5 } },
				}
			},
		}
	},
	ActiveTool = "kas_IridescentBlue"
}